#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

References:
- Implementation of a decorator with optional arguments:
  https://stackoverflow.com/questions/4486499/python-decorator-with-options#4486616
- List of Python decorators:
  https://wiki.python.org/moin/PythonDecoratorLibrary

"""
# import sys
import numpy as np
# import collections
# import functools
# import pickle
import logging
from datetime import datetime
from decorator import decorator

logger = logging.getLogger(__name__)


@decorator
def vectorize(func, signature=None, excluded=None, *args, **kwargs):
    """Vectorize a function. Allows optional arguments"""
    func = np.vectorize(func, signature=signature, excluded=excluded)
    return func(*args, **kwargs)


# def vectorize(signature=None, excluded=None):
#     """Vectorize a function. Allows optional arguments
#     to be passed to the decorator"""
#     def _vectorize(func):
#         def nfunc(*args, **kwargs):
#             return func(*args, **kwargs)
#         return np.vectorize(func, signature=signature, excluded=excluded)
#     return _vectorize


@decorator
def log_function(func, log_level='info', *args, **kwargs):
    def _logger(level):
        if level.lower() == 'debug':
            return logger.debug
        elif level.lower() == 'info':
            return logger.info
        elif level.lower() == 'warning':
            return logger.warning
        elif level.lower() == 'error':
            return logger.error
    _logger(log_level)(f'run {func.__name__}()...')
    t0 = datetime.now()
    try:
        result = func(*args, **kwargs)
    except Exception as exc:
        dt = datetime.now() - t0
        _logger('error')(f'run {func.__name__}()... FAIL (duration: {dt})')
        raise exc
    dt = datetime.now() - t0
    _logger(log_level)(f'run {func.__name__}()... DONE (duration: {dt})')
    return result


# class memoize(dict):
#     def __init__(self, func):
#         self.func = func

#     def __call__(self, *args):
#         return self[args]

#     def __missing__(self, key):
#         result = self[key] = self.func(*key)
#         return result

# def memoize(obj):
#     cache = obj.cache = {}

#     @functools.wraps(obj)
#     def memoizer(*args, **kwargs):
#         key = str(args) + str(kwargs)
#         if key not in cache:
#             cache[key] = obj(*args, **kwargs)
#         return cache[key]
#     return memoizer
