#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A collection of utility functions for xarray. It is save to import *.
"""
import os
import logging
import subprocess
import datetime
import regionmask
import warnings
import numpy as np
import xarray as xr
import __main__ as main
from scipy import signal, stats
# import salem

from .decorators import vectorize
from .physics import area_weighted_mean as area_weighted_mean_data

logger = logging.getLogger(__name__)

__all__ = [
    'get_variable_name',
    'get_longitude_name',
    'get_latitude_name',
    'antimeridian_pacific',
    'flip_antimeridian',
    'standardize_time',
    'standardize_dimensions',
    'area_weighted_mean',
    'select_region',
    'save_compressed',
    'add_hist',
    'detrend',
    'get_time_name',
    'remove_seasonal_cycle',
    'trend',
]

VALID_LON_UNITS = [
    'degree_east',
    'degrees_east',
]
VALID_LAT_UNITS = [
    'degree_north',
    'degrees_north',
]
VALID_TIME_UNITS = [
    'seconds since',
    'minutes since',
    'hours since',
    'days since',
    'years since',
]


def get_variable_name(ds):
    """Try to get the main variable from a dataset.

    This function tries to get the main variable from a dataset by checking the
    following properties:
    - the variable must NOT be a coordinate variable
    - the variable name must NOT contain '_bounds' or '_bnds'
    - There MUST be exactly one variable fulfilling the above

    Parameters
    ----------
    ds : xarray.Dataset
        Dataset with only one main variable, e.g., tas (time, lat, lon)
    """

    varns = [varn for varn in set(ds.variables).difference(ds.coords)
             if '_bounds' not in varn and '_bnds' not in varn]
    if len(varns) != 1:
        errmsg = 'Unable to select a single variable from {}'.format(
            ', '.join(varns))
        raise ValueError(errmsg)
    return varns[0]


def get_longitude_name(ds):
    """Get the name of the longitude dimension by CF unit"""
    lonn = []
    for dimn in ds.dims:
        if ('units' in ds[dimn].attrs and
            ds[dimn].attrs['units'] in VALID_LON_UNITS):
            lonn.append(dimn)
    if len(lonn) == 1:
        return lonn[0]
    elif len(lonn) > 1:
        errmsg = 'More than one longitude coordinate found by unit.'
    else:
        errmsg = 'Longitude could not be identified by unit.'
    raise ValueError(errmsg)


def get_latitude_name(ds):
    """Get the name of the latitude dimension by CF unit"""
    latn = []
    for dimn in ds.dims:
        if ('units' in ds[dimn].attrs and
            ds[dimn].attrs['units'] in VALID_LAT_UNITS):
            latn.append(dimn)
    if len(latn) == 1:
        return latn[0]
    elif len(latn) > 1:
        errmsg = 'More than one latitude coordinate found by unit.'
    else:
        errmsg = 'Latitude could not be identified by unit.'
    raise ValueError(errmsg)


def get_time_name(ds):
    """Get the name of the time dimension by CF unit"""
    timen = []
    for dimn in ds.dims:
        if ('units' in ds[dimn].attrs and
            ' '.join(ds[dimn].attrs['units'].split()[:2]) in VALID_TIME_UNITS):
            timen.append(dimn)
        elif ('units' in ds[dimn].encoding and
              ' '.join(ds[dimn].encoding['units'].split()[:2]) in VALID_TIME_UNITS):
            timen.append(dimn)
    if len(timen) == 1:
        return timen[0]
    elif len(timen) > 1:
        errmsg = 'More than one time coordinate found by unit.'
    else:
        errmsg = 'Time could not be identified by unit.'
    raise ValueError(errmsg)


def antimeridian_pacific(ds, lonn=None):
    """Returns True if the antimeridian is in the Pacific (i.e. longitude runs
    from -180 to 180."""
    if lonn is None:
        lonn = get_longitude_name(ds)
    if ds[lonn].min() < 0 or ds[lonn].max() < 180:
        return True
    return False


def flip_antimeridian(ds, to='Pacific', lonn=None):
    """
    Flip the antimeridian (i.e. longitude discontinuity) between Europe
    (i.e., [0, 360)) and the Pacific (i.e., [-180, 180)).

    Parameters:
    - ds (xarray.Dataset or .DataArray): Has to contain a single longitude
      dimension.
    - to='Pacific' (str, optional): Flip antimeridian to one of
      * 'Europe': Longitude will be in [0, 360)
      * 'Pacific': Longitude will be in [-180, 180)
    - lonn=None (str, optional): Name of the longitude dimension. If None it
      will be inferred by the CF convention standard longitude unit.

    Returns:
    same type as input ds
    """
    if lonn is None:
        lonn = get_longitude_name(ds)

    attrs = ds[lonn].attrs

    if to.lower() == 'europe' and not antimeridian_pacific(ds):
        return ds  # already correct, do nothing
    elif to.lower() == 'pacific' and antimeridian_pacific(ds):
        return ds  # already correct, do nothing
    elif to.lower() == 'europe':
        ds = ds.assign_coords(**{lonn: (ds.lon % 360)})
    elif to.lower() == 'pacific':
        ds = ds.assign_coords(**{lonn: (((ds.lon + 180) % 360) - 180)})
    else:
        errmsg = 'to has to be one of [Europe | Pacific] not {}'.format(to)
        raise ValueError(errmsg)

    was_da = isinstance(ds, xr.core.dataarray.DataArray)
    if was_da:
        da_varn = ds.name
        enc = ds.encoding
        ds = ds.to_dataset()

    idx = np.argmin(ds[lonn].data)
    varns = [varn for varn in ds.variables if lonn in ds[varn].dims]
    for varn in varns:
        if xr.__version__ > '0.10.8':
            ds[varn] = ds[varn].roll(**{lonn: -idx}, roll_coords=False)
        else:
            ds[varn] = ds[varn].roll(**{lonn: -idx})

    ds[lonn].attrs = attrs
    if was_da:
        da = ds[da_varn]
        da.encoding = enc
        return da
    return ds


# TODO: untested
# merged from 'blocking'
def sort_latitude(ds, ascending=True):
    """Sort the latitudes"""
    latn = get_latitude_name(ds)
    is_ascending = np.all(ds[latn][1:] - ds[latn][:-1] > 0.)
    if ascending == is_ascending:  # already correct, do nothing
        return ds
    else:
        return ds.sortby(latn, ascending=ascending)


# TODO: untested
def standardize_time(ds, inplace=False, timen='time', **kwargs):
    """Standardize time values

    Due to time averaging some models have slightly different time values,
    such as 2000-01-01 12:00:00.00 and 2000-01-01 13:00:00.00 in monthly
    aggregated data. This prevents merging with xarray.concat() since
    they will be interpreted as different times. This function does some
    checks and standardizes the time to the given values.

    Parameters
    ----------
    ds : {xarray.Dataset, xarray.DataArray}
        Input dataset, needs to contain the time dimension.
    inplace : bool, optional
        If false return a copy with times standardized.
    timen : str, optional
        The time dimension name
    **kwargs : any combination of valid cftime.datetime key value pairs, optional
        If not set then standard values will be used depending on the time
        resolution of ds. If set only keys given in kwargs will be
        standardized. If kwargs contains a key which would set a value larger
        or equal to the time resolution of ds a ValueError will be raised
        (e.g., setting day=15 for daily data will fail since it would
        potentially lead to ambiguous results).

    Returns
    -------
    ds_standardized : same type as ds
        A dataset where the time dimension values have been standardized.

    Defaults
    --------
    The default is to set the date to the approximate middle of the
    corresponding period, i.e.,
    - annual: yyyy-06-15 12:00:00
    - monthly: yyyy-mm-15 12:00:00
    - daily: yyyy-mm-dd 12:00:00
    - sub-daily: return untouched

    Examples
    --------
    >> ds = xr.Dataset(coords={'time_name': xr.cftime_range(
           start='2000-01-01T13:15:30',
           end='2000-01-2T13:15:30',
           freq='1D')})
    >> ds['time_name']
    <xarray.DataArray 'time_name' (time_name: 2)>
    array([cftime.DatetimeProlepticGregorian(2000, 1, 1, 13, 15, 30, 0, -1, 1),
           cftime.DatetimeProlepticGregorian(2000, 1, 2, 13, 15, 30, 0, -1, 1)],
          dtype=object)
    >> standardize_time(ds, timen='time_name').time_name
    <xarray.DataArray 'time_name' (time_name: 2)>
    array([cftime.DatetimeProlepticGregorian(2000, 1, 1, 0, 0, 0, 0, -1, 1),
           cftime.DatetimeProlepticGregorian(2000, 1, 2, 0, 0, 0, 0, -1, 1)])
    >> standardize_time(ds, timen='time_name', hour=12).time_name  # only hour, rest untouched
    <xarray.DataArray 'time_name' (time_name: 2)>
    array([cftime.DatetimeProlepticGregorian(2000, 1, 1, 12, 15, 30, 0, -1, 1),
           cftime.DatetimeProlepticGregorian(2000, 1, 2, 12, 15, 30, 0, -1, 1)])
    """
    if timen not in ds.dims:
        raise ValueError(f'{timen} not found in ds')

    dtimes = (ds[timen].data[1:]-ds[timen].data[:-1]).astype('timedelta64[D]')

    if np.any(dtimes < np.timedelta64(1, 'D')):  # resolution smaller 1 day
        logmsg = 'Time resolution smaller 1 day, standardizing not possible'
        logger.warning(logmsg)
        if not inplace:
            return ds
        return None

    if np.all(dtimes >= np.timedelta64(365, 'D')):
        logmsg = 'All time steps larger than one year, assume annual files'
        logger.debug(logmsg)
        if len(kwargs) == 0:  # if no kwargs are give use default
            kwargs = dict(
                month=6,
                day=15,
                hour=12,
                minute=0,
                second=0,
                microsecond=0)
        else:  # check if user input is valid
            allowed_keys = ['month', 'day', 'hour', 'minute', 'second', 'microsecond']
            if not np.all([key in allowed_keys for key in kwargs.keys()]):
                errmsg = 'Can not standardize {} for resolution > monthly'.format(
                    ', '.join(set(kwargs.keys()).difference(allowed_keys)))
                raise ValueError(errmsg)
    elif np.all(dtimes >= np.timedelta64(28, 'D')):
        logmsg = 'All time steps larger than 27 days, assume monthly files'
        logger.debug(logmsg)
        if len(kwargs) == 0:
            kwargs = dict(
                day=15,
                hour=12,
                minute=0,
                second=0,
                microsecond=0)
        else:
            allowed_keys = ['day', 'hour', 'minute', 'second', 'microsecond']
            if not np.all([key in allowed_keys for key in kwargs.keys()]):
                errmsg = 'Can not standardize {} for resolution > monthly'.format(
                    ', '.join(set(kwargs.keys()).difference(allowed_keys)))
                raise ValueError(errmsg)
    else:
        logmsg = 'All time steps larger than one day, assume daily files'
        logger.debug(logmsg)
        if len(kwargs) == 0:  # if no kwargs are give use standard settings
            kwargs = dict(
                hour=12,
                minute=0,
                second=0,
                microsecond=0)
        else:
            allowed_keys = ['hour', 'minute', 'second', 'microsecond']
            if not np.all([key in allowed_keys for key in kwargs.keys()]):
                errmsg = 'Can not standardize {} for daily resolution'.format(
                    ', '.join(set(kwargs.keys()).difference(allowed_keys)))
                raise ValueError(errmsg)

    if not inplace:
        ds = ds.copy()

    ds[timen].data = [time.replace(**kwargs) for time in ds[timen].to_index()]

    if not inplace:
        return ds


# TODO: untested
# merged from 'blocking'
def standardize_dimensions(ds, latn='lat', lonn='lon', timen='time',
                           antimeridian='Pacific',
                           lat_ascending=True, time={}):
    """Standardize the dimensions of a ds to the given values.
    All parameters are optional and can be set to None to be skipped.

    Parameters
    ----------
    ds : {xarray.Dataset, xarray.DataArray}
        Input dataset, needs to contain latitude and longitude dimensions.
    latn : str, optional
        Name of the new latitude dimension.
    lonn : str, optional
        Name of the new longitude dimension.
    timen : str, optional
        Name of the new time dimension.
    antimeridian : {'Pacific', 'Europe'}, optional
        New location of the antimeridian.
    lat_ascending : bool, optional
        New order of the latitude dimension.
    time : dict, optional
        Passed to standardize_time (see docstring for more information)

    Returns
    -------
    ds_standardized : same type as input
        A dataset where some dimensions have been standardized.
    """
    if latn is not None:
        ds = ds.rename({get_latitude_name(ds): latn})
    if lonn is not None:
        ds = ds.rename({get_longitude_name(ds): lonn})
    if timen is not None:
        ds = ds.rename({get_time_name(ds): timen})
    if antimeridian is not None:
        ds = flip_antimeridian(ds, to=antimeridian)
    if lat_ascending is not None:
        ds = sort_latitude(ds, ascending=lat_ascending)
    if time is not None:
        ds = standardize_time(ds, **time)
    return ds


def area_weighted_mean(
        ds, latn=None, lonn=None, keep_attrs=True, suppress_warning=False):
    """xarray version of utils_python.physics.area_weighed_mean

    Parameters
    ----------
    ds : {xarray.Dataset, xarray.DataArray}
        Has to contain at least latitude and longitude dimensions
    lonn : string, optional
    latn : string, optional
    keep_attrs : bool, optional
        Whether to keep the global attributes
    suppress_warning : bool, optional
        Suppress warnings about nan-only instances

    Returns
    -------
    mean : same type as input with (lat, lon) dimensions removed
    """
    if suppress_warning:
        warnings.simplefilter('ignore')
    if latn is None:
        latn = get_latitude_name(ds)
    if lonn is None:
        lonn = get_longitude_name(ds)

    was_da = isinstance(ds, xr.core.dataarray.DataArray)
    if was_da:
        ds = ds.to_dataset(name='data')

    ds_mean = ds.mean((latn, lonn), keep_attrs=keep_attrs)  # create this just to fill
    for varn in set(ds.variables).difference(ds.dims):
        if latn in ds[varn].dims and lonn in ds[varn].dims:
            var = ds[varn].data
            axis_lat = ds[varn].dims.index(latn)
            axis_lon = ds[varn].dims.index(lonn)
            var = np.moveaxis(var, (axis_lat, axis_lon), (-2, -1))
            mean = area_weighted_mean_data(var, ds[latn].data, ds[lonn].data)
        elif latn in ds[varn].dims:
            var = ds[varn].data
            axis_lat = ds[varn].dims.index(latn)
            var = np.moveaxis(var, axis_lat, -1)
            mean = area_weighted_mean_data(var, ds[latn].data)
        elif lonn in ds[varn].dims:
            mean = ds[varn].mean(lonn).data
        else:
            continue

        if '_FillValue' in ds[varn].encoding.keys():
            fill_value = ds[varn].encoding['_FillValue']
            if fill_value != np.nan:
                mean = np.where(np.isnan(mean), fill_value, mean)
        ds_mean[varn].data = mean

    warnings.resetwarnings()
    if was_da:
        return ds_mean['data']
    return ds_mean


def save_compressed(ds, path, log=True):
    if log:
        logger.info('Save & compress file...')
    ds.to_netcdf(path)
    fs_org = os.path.getsize(path) / 1024.**2
    subprocess.run(['/usr/bin/nccopy', '-d9',
                    path, path.replace('.nc', '_temp.nc')])
    os.remove(path)
    os.rename(path.replace('.nc', '_temp.nc'), path)
    if log:
        fs_com = os.path.getsize(path) / 1024.**2
        logmsg = 'Save & compress file... DONE ({:.1f}MB -> {:.1f}MB)'.format(
            fs_org, fs_com)
        logger.info(logmsg)


def add_hist(ds, warning=True, attr='history'):
    """
    Add script name to the netCDF history attribute similar to cdo.

    Parameters
    ----------
    ds : xarray.Dataset
    warning : bool, optional
        Log a warning if the repository is not under Git control.

    Returns
    -------
    ds : same as input

    Format
    ------
    Git:
    time_stamp git@url:git_rep.git /relative/path/script.py, tag: ##, branch: branch_name

    time_stamp :  The current time
    git@url:git_rep.git : Origin of the git reposiroty
        git config --get remote.origin.url
    /relative/path/script.py : relative path
        git ls-tree --full-name --name-only HEAD
    ## : revision hash or revision hash
        git describe --always
    branch_name : currently checked out branch
        git rev-parse --abbrev-ref HEAD
    If the main calling script checked in a git repository 'git@url:git_rep.git' like
    (branch_name)~ ./code/script.py

    Not in Git:
    'time_stamp /absolute/path/script.py'
    """
    time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    filename = str(os.path.realpath(main.__file__))
    try:
        revision = subprocess.check_output([
            'git', 'describe', '--always']).strip().decode()
        rep_name = subprocess.check_output([
            'git', 'config', '--get', 'remote.origin.url']).strip().decode()
        branch_name = subprocess.check_output([
            'git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip().decode()
        filename_git = subprocess.check_output([
            'git', 'ls-tree', '--full-name',
            '--name-only', 'HEAD', filename]).strip().decode()
        if filename_git == '':
            if warning:
                logmsg = '{} is in a Git repository but not checked in!'.format(
                    filename)
                logger.warning(logmsg)
            str_add = '{} {}'.format(time, filename)
        else:
            str_add = '{} {}: {}, revision hash: {}, branch: {}'.format(
                time, rep_name, filename_git, revision, branch_name)
    except subprocess.CalledProcessError:
        if warning:
            logmsg = '{} is not under Git control!'.format(filename)
            logger.warning(logmsg)
        str_add = '{} {}'.format(time, filename)

    if attr in ds.attrs:
        ds.attrs[attr] = '{} {}'.format(ds.attrs[attr], str_add)
    else:
        ds.attrs[attr] = str_add
    return ds


def add_revision(ds, attr_name):
    return add_hist(ds, warning=True, attr=attr_name)


def select_region(da, region=None, country=None, mask_sea=False, average=False,
                  grid_point=None, return_mask=False, input_mask=None):
    """Select a SREX region, country, or land mask my masking not-matching
    grid points. Note that region, country and mask_sea can be combined.
    Uses the regionmask package. Alternatively, select the nearest
    grid point to given coordinates. Can also average over the selected region.

    Parameters
    ----------
    da : {xarray.DataArray, xarray.Dataset}
        A xarray.DataArray object of a variable depending on latitude and
        longitude. Can also be a xarray.Dataset object if it contains only one
        not-coordinate variable.
    region : str or list of str, optional
        Region to select. If given has to be at least one of: ALA CGI WNA CNA
        ENA CAM AMZ NEB WSA SSA NEU CEU MED SAH WAF EAF SAF NAS WAS CAS TIB EAS
        SAS SEA NAU SAU
    country : str or list of str, optional
        Country to select.
        See regionmask.defined_regions.natural_earth.countries_110[.names] for
        a list of valid country names.
    mask_sea : bool, optional
        If true the ocean will be masked.
    average : bool, optional
        If true the average over the regions will be taken.
    grid_point : tuple, optional
        tuple containing latitude and longitude and optionally an identifier
        string (lat, lon[, str]). Should not be combined with any other kwarg!
        This will select the grid point closest to the given coordinates.
    return_mask : bool, optional
        If True return the mask instead of applying it.
    input_mask : str or xarray.DataArray or None, optional
        Option to apply a input mask instead of creating one. NOT IMPLEMENTED

    Returns
    -------
    results : same type as input with regions selected/averaged
    if return_mask:
    mask : same type as input
        Returning the mask that otherwise would be applied to the data
    """
    wrap_lon = False
    if not antimeridian_pacific(da):
        wrap_lon = True

    if region is not None and country is not None:
        # masking region and country probably does not make sense
        raise ValueError('Can not mask region and country at the same time')

    varn = None
    if isinstance(da, xr.core.dataset.Dataset):
        varn = get_variable_name(da)
        attrs = da.attrs
        da = da[varn]
    var_attrs = da.attrs

    masks = []
    if region is not None:
        if isinstance(region, str):
            region = [region]

        keys = regionmask.defined_regions.srex.map_keys(region)
        for key in keys:
            masks.append(
                regionmask.defined_regions.srex.mask(da, wrap_lon=wrap_lon) == key)

    if country is not None:
        if isinstance(country, str):
            country = [country]

        keys = regionmask.defined_regions.natural_earth.countries_50.map_keys(
            country)
        for key in keys:
            masks.append(
                regionmask.defined_regions.natural_earth.countries_50.mask(
                    da, wrap_lon=wrap_lon) == key)

    mask = sum(masks) > 0

    if mask_sea:
        sea_mask = regionmask.defined_regions.natural_earth.land_110.mask(
            da, wrap_lon=wrap_lon) == 0  # zero is land
        if len(masks) != 0:
            # mask = True values are kept!! Only select when both are True!
            mask = (mask.astype(int) + sea_mask.astype(int)) == 2
        else:
            mask = sea_mask

    if np.all(mask):
        logmsg = 'All grid points masked. Wrong combination of masks?'
        logger.warning(logmsg)

    if return_mask:
        if not isinstance(mask, bool):
            mask.name = 'mask'
            mask.attrs = {
                'description': 'Based on Natural Earth: https://www.naturalearthdata.com;',
                'flag_values': '0, 1',
                'flag_meaning': 'out_of_region, in_region'}
            mask[get_latitude_name(da)].attrs = {'units': 'degree_north'}
            mask[get_longitude_name(da)].attrs = {'units': 'degree_east'}
            if varn is not None:
                mask = mask.to_dataset()
                mask.attrs = attrs
        return mask

    if np.any(mask):
        name = da.name
        da = da.where(mask)
        da.name = name

    if grid_point is not None:
        idx_lat = np.argmin(np.abs(da['lat'].data - grid_point[0]))
        idx_lon = np.argmin(np.abs(da['lon'].data - grid_point[1]))
        da = da.isel(lat=idx_lat).isel(lon=idx_lon)
        if np.any(np.isnan(da.data)):
            raise ValueError('grid_point is nan. Masked by sea mask?')
        if len(grid_point) == 3:
            da.attrs['location'] = str(grid_point[2])

    if average and grid_point is None:
        da = area_weighted_mean(da)

    da.attrs = var_attrs
    if varn is not None:
        da = da.to_dataset(name=varn)
        da.attrs = attrs
    return da


def detrend(da, time_name='time'):
    """Remove the linear trend from the time dimension"""
    def detrend_data(data):
        if np.any(np.isnan(data)):
            return data * np.nan
        return signal.detrend(data)

    return xr.apply_ufunc(
        detrend_data, da,
        input_core_dims=[[time_name]],
        output_core_dims=[[time_name]],
        vectorize=True,
        keep_attrs=True)


def trend(da, time_name='time'):
    """Calculate the linear least-squares trend"""
    def trend_data(data):
        if np.any(np.isnan(data)):
            return np.nan
        return stats.linregress(np.arange(len(data)), data)[0]

    return xr.apply_ufunc(
        trend_data, da,
        input_core_dims=[[time_name]],
        vectorize=True,
        keep_attrs=False)


# NOTE: comment out since salem is not yet a standard package
# def auto_cut_region(ds, margin=0):
#     """Remove all latitudes and longitude containing only NaN values.

#     Parameters
#     ----------
#     ds : xarray.DataArray
#     margin : int, optional
#         Don't remove `margin` grid cells on each side.

#     Returns
#     -------
#     cut_ds : same type as input
#     """
#     return ds.salem.subset(roi=~np.isnan(ds), margin=margin)


def remove_seasonal_cycle(ds, clim=None, time_name='time', period_slice=None):
    """Remove the monthly climatology from each respective month.

    Parameters
    ----------
    ds : {xarray.Dataset, xarray.DataArray}
    clim : {xarray.Dataset, xarray.DataArray}, optional
        Climatology to use for the removal of the seasonal cycle. If None ds
        will be used.
    time_name : string, optional
    period_slice : slice, optional
        If not None (default) has to be a valid xarray time slice giving the
        period over which the climatology is calculated. E.g.,
        slice('1995-01-01', '2014')
        WARNING: If clim does not cover the full period_slice this function
        will silently only select the overlapping part (see xarray.Dataset.sel)

    Return
    ------
    deseas : same type as input
    """
    if clim is None:
        clim = ds
    if period_slice is not None:
        # TODO: implement a check if full period is in clim?
        clim = clim.sel(**{time_name: period_slice})
    # calculate the climatological mean for each month
    clim = clim.groupby(f'{time_name}.month').mean(time_name)

    def remove_monthly_clim(x):
        """Remove the climatology for each month"""
        month = np.unique(x[time_name].dt.month)  # need to get month info
        assert len(month) == 1
        return x - clim.sel(month=month[0])

    deseas = ds.groupby(f'{time_name}.month').apply(remove_monthly_clim)
    del deseas['month']
    return deseas
