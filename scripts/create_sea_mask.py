#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-10-25 16:15:56 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Creates a land-sea mask file based on a given input grid.

Properties
----------
- Masks according to the grid cell center! I.e., grid cells with more than 50%
  land might be masked if their center is over the ocean and vice versa.
- The Caspian Sea is not masked
- Based on Natural Earth 1:110m

References
----------
https://www.naturalearthdata.com/
https://regionmask.readthedocs.io/en/stable/defined_landmask.html

"""
import os
import logging
import argparse
import regionmask
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

from utils_python.utils import set_logger, log_parser
from utils_python.xarray import (get_variable_name,
                                 get_longitude_name,
                                 get_latitude_name,
                                 flip_antimeridian,
                                 add_hist)

DEFAULT_MASK_FILE = '/net/atmos/data/cmip5-ng/tas/tas_ann_NorESM1-M_rcp85_r1i1p1_g025.nc'

def get_mask(da):
    latn = get_latitude_name(da)
    lonn = get_longitude_name(da)
    da = flip_antimeridian(da)
    mask = regionmask.defined_regions.natural_earth.land_110.mask(da) == 0
    mask.name = 'land_sea_mask'
    mask.attrs = {
        'long_name': 'Land-sea mask',
        'description': ' '.join([
        'Based on Natural Earth: https://www.naturalearthdata.com;',
            'The Caspian Sea is not masked!']),
        'flag_values': '0, 1',
        'flag_meaning': 'ocean, land'}

    ds = mask.to_dataset()
    ds[latn].attrs = {'units': 'degree_north'}
    ds[lonn].attrs = {'units': 'degree_east'}
    add_hist(ds)
    return ds


def get_grid(fn, varn=None):
    da = xr.open_dataset(fn)
    if varn is None:
        varn = get_variable_name(da)
    return da[varn]


def plot(ds):
    proj = ccrs.PlateCarree(central_longitude=0)
    fig, ax = plt.subplots(subplot_kw={'projection': proj})
    ds['land_sea_mask'].plot.pcolormesh(
        ax=ax,
        transform=ccrs.PlateCarree(),
        add_colorbar=False)
    ax.coastlines()
    xx, yy = np.meshgrid(ds['lon'], ds['lat'])
    ax.scatter(xx, yy, s=1, color='k')

    plt.show()


def read_config():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        dest='filename', default=DEFAULT_MASK_FILE, nargs='?', type=str,
        help=' '.join([
            'A file with a latitude-longitude grid as basis for the mask',
            'Defaults to {}'.format(DEFAULT_MASK_FILE)]))
    parser.add_argument(
        '--variable-name', '-v', dest='varn', default=None, type=str,
        help=' '.join([
            'If filename contains more than one variable varn has to be given',
            'and point to a variable depending on latitude-longitude.']))
    parser.add_argument(
        '--save-path' '-s', dest='save_path', default=None, type=str,
        help='Location to save the mask. Defaults to ./mask.nc')
    parser.add_argument(
        '--plot', '-p', dest='plot', action='store_true',
        help='Show a map for checking the mask.')
    args = parser.parse_args()
    log_parser(args)
    return args


if __name__ == '__main__':
    set_logger()
    args = read_config()
    da = get_grid(args.filename, varn=args.varn)
    ds = get_mask(da)

    if args.plot:
        plot(ds)

    if args.save_path is None:
        args.save_path = './land_sea_mask.nc'

    ds.to_netcdf(args.save_path)
