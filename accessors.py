#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2019-05-23 17:28:41 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import xarray as xr

from .xarray import *


class AccessorBase:

    def __init__(self, xarray_obj):
        self._obj = xarray_obj

    @property
    def lon_name(self):
        """Get the name of the longitude dimension"""
        return get_longitude_name(self._obj)

    @property
    def lat_name(self):
        """Get the name of the latitude dimension"""
        return get_latitude_name(self._obj)

    @property
    def time_name(self):
        """Get the name of the time dimension"""
        return get_time_name(self._obj)

    @property
    def var_name(self):
        """Get the name of the variable (if only one)"""
        return get_variable_name(self._obj)

    @property
    def to_dataarray(self):
        """Convert to a DataArray (if only one variable exists)"""
        return self._obj[get_variable_name(self._obj)]

    @property
    def lon_is180(self):
        """Check if longitudes in [-180, 180)"""
        return antimeridian_pacific(self._obj)

    @property
    def lon_is360(self):
        """Check if longitudes in [0, 360)"""
        return not antimeridian_pacific(self._obj)

    @property
    def lon_to180(self):
        """Map longitudes to [-180, 180)"""
        return flip_antimeridian(self._obj)

    @property
    def lon_to360(self):
        """Map longitudes to [0, 360)"""
        return flip_antimeridian(self._obj, to='Europe')

    @property
    def latlon_mean(self):
        """Calculates an area-weighted mean over lat & lon"""
        return area_weighted_mean(self._obj)

    @property
    def standardize_time(self):
        """Standardize the time dimension"""
        return standardize_time(self._obj)

    @property
    def standardize_dimensions(self):
        """Standardize several main dimensions"""
        return standardize_dimensions(self._obj)

    @property
    def deseasonalize(self):
        """Remove the mean over all years from each month separately"""
        return remove_seasonal_cycle(self._obj)


@xr.register_dataarray_accessor('clim')
class DataArrayAccessors(AccessorBase):

    def select(self, **kwargs):
        """
        select_region(da, region=None, country=None, mask_sea=False,
                      average=False, grid_point=None, return_mask=False,
                      input_mask=None)

        Cut a mask by region, country, land or select a grid point
        """
        return select_region(self._obj, **kwargs)

    @property
    def detrend(self):
        """Remove the linear trend from the time dimension"""
        return detrend(self._obj)

    @property
    def trend(self):
        """Calculate the linear trend along the time dimension"""
        return trend(self._obj)


@xr.register_dataset_accessor('clim')
class DatasetAccessors(AccessorBase):

    @property
    def add_hist(self):
        """Add script information to the history attribute"""
        add_hist(self._obj)

    def to_netcdf(self, path):
        """Save, then compress with nccopy -d9"""
        save_compressed(self._obj, path)
