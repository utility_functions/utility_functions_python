#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2019-06-18 14:09:27 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import numpy as np
import xarray as xr
import seaborn as sns
import matplotlib as mpl
from scipy.stats import norm
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle, Patch, Polygon
import matplotlib.patches as mpatches
from matplotlib.path import Path
from statsmodels.stats.weightstats import DescrStatsW
import statsmodels.api as sm
from scipy import stats
from dataclasses import dataclass

from .math import quantile2


@dataclass
class Colors:

    color_names = [
        'darkred',
        'firebrick',
        'crimson',
        'orangered',
        'indianred',
        'tomato',
        'salmon',
        'lightsalmon',
        'darksalmon',
        'peru',
        'goldenrod',
        'rosybrown',
        'tan',
        'beige',
        'palegoldenrod',
        'khaki',
        'darkkhaki',
        'olive',
        'olivedrab',
        'forestgreen',
        'darkgreen',
        'mediumseagreen',
        'aquamarine',
        'lightseagreen',
        'mediumturquoise',
        'paleturquoise',
        'lightblue',
        'lightskyblue',
        'dodgerblue',
        'steelblue',
        'royalblue',
        'darkslateblue',
        'rebeccapurple',
        'blueviolet',
        'darkorchid',
        'purple',
        'orchid',
        'magenta',
        'hotpink',
        'lightpink',
        'palevioletred',
        'mediumvioletred',
    ]

    def __init__(self, nn=None, repeat=False, random_seed=0):
        self.colors = self._colors(nn, random_seed)
        self.repeat = repeat
        self.idx = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.idx >= len(self.colors):
            if not self.repeat:
                raise StopIteration
            self.idx = 0
        self.idx += 1
        return self.colors[self.idx-1]

    @property
    def get(self):
        return self.__next__()

    @property
    def get_last(self):
        self.idx -= 1
        return self.get

    @property
    def reset(self):
        self.idx = 0

    @classmethod
    def _colors(cls, nn=None, random_seed=0):
        if nn is None or nn >= len(cls.color_names):
            return sns.color_palette(np.array(cls.color_names))
        np.random.seed(random_seed)
        idx = np.random.choice(range(len(cls.color_names)), nn, replace=False)
        return sns.color_palette(np.array(cls.color_names)[np.sort(idx)])


def get_color_list(nn=None, names=False):
    """A convenience function returning ~40 user defined nicely-looking colors.

    Parameters:
    - nn=None (int, optional): Number of colors to return. If less than
      available they will be stretched over the full range (i.e., some
      colors 'in the middle' will be missing).
    - names=False (bool, optional): Return name strings instead of colors.

    Returns:
    seaborn.color_palette OR list of str"""

    color_names = [
        'darkred',
        'firebrick',
        'crimson',
        'orangered',
        'indianred',
        'tomato',
        'salmon',
        'lightsalmon',
        'darksalmon',
        'peru',
        'goldenrod',
        'rosybrown',
        'tan',
        'beige',
        'palegoldenrod',
        'khaki',
        'darkkhaki',
        'olive',
        'olivedrab',
        'forestgreen',
        'darkgreen',
        'mediumseagreen',
        'aquamarine',
        'lightseagreen',
        'mediumturquoise',
        'paleturquoise',
        'lightblue',
        'lightskyblue',
        'dodgerblue',
        'steelblue',
        'royalblue',
        'darkslateblue',
        'rebeccapurple',
        'blueviolet',
        'darkorchid',
        'purple',
        'orchid',
        'magenta',
        'hotpink',
        'lightpink',
        'palevioletred',
        'mediumvioletred',
    ]

    if nn is not None and nn < len(color_names):
        np.random.seed(0)
        idx = np.random.choice(range(len(color_names)), nn, replace=False)
        idx.sort()
    elif nn is not None and nn > len(color_names):
        raise NotImplementedError
    else:
        idx = np.arange(len(color_names))

    if names:
        return list(np.array(color_names)[idx])
    return sns.color_palette(np.array(color_names)[idx])


def histogram(
        ax,
        data,
        weights=None,
        labels=None,
        title='',
        xlabel='',
        ylabel='',
        xlim=None,
        colors=None,
        plot_hist=True,
        plot_pdf=True,
        plot_density=False,
        bin_width=.5,
        alpha=.5,
        bin_numbers=False,
):
    """Plot histograms and corresponding PDFs for weighted data.

    Parameters
    ----------
    data : array_like (N,) or tuple of array_like
    weights : None or array_like (N,) or tuple of same shape as data tuple, optional
        If None equal weights will be used.
    labels : None or str or tuple of str same shape as data tuple, optional
        If None the label will be skipped, if all labels are None no legend
        will be plotted.
    title : str, optional
    xlabel, ylabel : str, optinoal
    colors : None or tuple of valid colors same shape as data tuple, optional
        If None a sequence of colors will be created.
    plot_hist, plot_pdf : bool or tuple of same shape as data tuple, optional
        If only value it will be taken for all elements of data tuple.
    bin_width : float, optional
    alpha : float or tuple of float same shape as data tuple, optional
        If only value it will be taken for all elements of data tuple.
    bin_numbers : bool, optional
        A convenience function which shows the number of each bin in the plot.

    Returns
    -------
    None
    """

    if not isinstance(data, tuple):
        data = (data,)
    if not isinstance(weights, tuple):
        weights = (weights,)
    if labels is None:
        labels = (None,) * len(data)
    elif not isinstance(labels, tuple):
        labels = (labels,)
    if isinstance(plot_hist, bool):
        plot_hist = (plot_hist,) * len(data)
    if isinstance(plot_pdf, bool):
        plot_pdf = (plot_pdf,) * len(data)
    if isinstance(plot_density, bool):
        plot_density = (plot_density,) * len(data)
    if isinstance(alpha, float):
        alpha = (alpha,) * len(data)
    if colors is None:
        colors = get_color_list(len(data))

    assert len(data) == len(weights)
    assert len(data) == len(labels)
    assert len(data) == len(plot_hist)
    assert len(data) == len(plot_pdf)
    assert len(data) == len(plot_density)
    assert len(data) == len(alpha)
    assert len(data) == len(colors)

    labels = list(labels)  # labels needs to support idem assignment

    min_, max_ = np.floor(np.min(data)), np.ceil(np.max(data))
    bins = np.arange(min_, max_, bin_width)

    def _hist(data, weights, label, color, alpha):
        return ax.hist(
            x=data,
            weights=weights,
            color=color,
            bins=bins,
            alpha=alpha,
            density=True,
            zorder=10)

    def _pdf(data, weights, label, color):
        if weights is None:
            weights = np.ones_like(data)
        data_stats = DescrStatsW(data, weights=weights)
        xx = np.linspace(min_-5, max_+5, 1000)
        return ax.plot(
            xx,
            norm.pdf(xx, data_stats.mean, data_stats.std),
            color=color,
            zorder=20), data_stats.mean, data_stats.std

    def _density(data, weights, label, color):
        density = sm.nonparametric.KDEUnivariate(data,)
        density.fit(fft=False, weights=weights, adjust=.75)
        return ax.plot(
            density.support,
            density.density,
            color=color,
            ls=':',
            zorder=20,
        )

    label_handlers = np.empty_like(labels, dtype=object)
    for idx, _ in enumerate(data):
        label_handler = ()
        if plot_hist[idx]:
            _, _, patch = _hist(data[idx], weights[idx], labels[idx], colors[idx], alpha[idx])
            label_handler += (patch[0],)
        if plot_pdf[idx]:
            line, mean, std = _pdf(data[idx], weights[idx], labels[idx], colors[idx])
            label_handler += (line[0],)
            if labels[idx] is not None:
                labels[idx] = '{}: {:.2f}$\pm${:.3f}'.format(labels[idx], mean, std)
        if plot_density[idx]:
            line = _density(data[idx], weights[idx], labels[idx], colors[idx])
            label_handler += (line[0],)
        label_handlers[idx] = label_handler

    label_handlers = np.delete(label_handlers, np.where(np.array(labels) == None)[0])
    labels = np.delete(labels, np.where(np.array(labels) == None)[0])

    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    ax.grid(zorder=0)
    if xlim is None:
        ax.set_xlim(min_-1, max_+1)
    else:
        ax.set_xlim(xlim)

    if len(labels) != 0:
        ax.legend(label_handlers, labels)

    if bin_numbers:
        for tt, xx in enumerate(bins[:-1]+.5*bin_width):
            ax.text(xx, 0., str(tt), fontsize='x-small', ha='center')

    return np.digitize(data, bins)


def boxplot(ax,
            pos=None,
            data=None,
            median=None,
            mean=None,
            box=None,
            whis=None,
            weights=None,
            width=.8,
            color=sns.xkcd_rgb['greyish'],
            alpha=1.,
            showcaps=True,
            fancy_legend=False,
            return_handle=False,
            zorder=100,
            median_kwargs=None,
            mean_kwargs=None,
            whis_kwargs=None):
    """
    A custom-mad boxplot routine based on user set statistics.

    Parameters
    ----------
    ax : plt.axis object
    pos : float, optional
        Location (center) of the box.
    data: array-like, optional
        An array of data to calculate the statistics from. If this is
        not None, median, mean, box, and whis will be overwritten.
    median : float or array-like, optional
        Location of the median or data to calculate the median.
    mean : float or array-like, optional
        Location of the mean or data to calculate the mean.
    box : tuple of float or array-like, shape (2), optional
        Extend of the box or data to calculate the box.
    whis : tuple of float or array-like, shape (2), optional
        Extend of the whiskers or data to calculate the whiskers.
    width : float, optional
        Width of box, median, mean, and caps (caps have .4*width).
    color : string, optional
        Box color and default color for median, mean, and whiskers.
    showcaps : bool, optional
        Whether to draw caps at the end of the whiskers.
    zorder : int, optional
        zorder of the drawn objects.
    median_kwargs : dict, optional
        Keyword arguments passed on to ax.hlines for the median.
    mean_kwargs : dict, optional
        Keyword arguments passed on to ax.hlines for the mean.
    whis_kwargs : dict, optional
        Keyword arguments passed on to ax.hlines and ax.vlines for whiskers
        and caps.
    """
    if data is not None:
        mean = data
        median = data
        box = data
        whis = data

    if mean is not None and not isinstance(mean, (int, float)):
        mean = np.average(mean, weights=weights)
    if median is not None and not isinstance(median, (int, float)):
        median = quantile2(median, .5, weights)
    if box is not None and len(box) != 2 and not isinstance(box, tuple):
        box = quantile2(box, (.25, .75), weights)
    elif box == (None, None):
        box = None
    if whis is not None and len(whis) != 2 and not isinstance(whis, tuple):
        whis = quantile2(whis, (.05, .95), weights)
    elif whis == (None, None):
        whis = None

    if pos is None:
        pos = 0
    if median_kwargs is None:
        median_kwargs = {}
    if mean_kwargs is None:
        mean_kwargs = {}
    if whis_kwargs is None:
        whis_kwargs = {}
    if 'colors' not in median_kwargs.keys():
        median_kwargs['colors'] = 'k'
    if 'colors' not in mean_kwargs.keys():
        mean_kwargs['colors'] = 'k'
    if 'colors' not in whis_kwargs.keys():
        whis_kwargs['colors'] = color
    if 'caps_width' in whis_kwargs.keys():
        caps_width = whis_kwargs.pop('caps_width')
    else:
        caps_width = .4
    if 'width' in mean_kwargs.keys():
        mean_width = mean_kwargs.pop('width')
    else:
        mean_width = 1.
    if 'width' in median_kwargs.keys():
        median_width = median_kwargs.pop('width')
    else:
        median_width = 1.
    if 'linestyle' not in mean_kwargs.keys():
        if median is not None:
            mean_kwargs['linestyle'] = '--'

    l1 = mpatches.Patch(color=color, alpha=alpha)
    l2 = ax.hlines([], [], [], **median_kwargs)
    l3 = ax.hlines([], [], [], **mean_kwargs)
    if return_handle:
        return (l1, l2, l3)

    x0, x1 = pos - .5*width, pos + .5*width
    if box is not None:  # plot box
        patch = PatchCollection(
            [Rectangle((x0, box[0]), width, box[1] - box[0])],
            facecolor=color,
            alpha=alpha,
            zorder=zorder)
        ax.add_collection(patch)

    if median is not None:
        x0_median = x0 + (1 - median_width)*width*.5
        x1_median = x1 - (1 - median_width)*width*.5
        ax.hlines(median, x0_median, x1_median, zorder=zorder, **median_kwargs)

    if mean is not None:  # plot mean
        x0_mean = x0 + (1 - mean_width)*width*.5
        x1_mean = x1 - (1 - mean_width)*width*.5
        ax.hlines(mean, x0_mean, x1_mean, zorder=zorder, **mean_kwargs)

    if whis is not None:  # plot whiskers
        if box is None:
            box = (whis[0], whis[0])
        ax.vlines((pos, pos), (whis[0], box[1]), (box[0], whis[1]),
                  zorder=zorder, **whis_kwargs)
        if showcaps:  # plot caps
            x0, x1 = pos - .5*caps_width*width, pos + .5*caps_width*width
            ax.hlines(whis, (x0, x0), (x1, x1), zorder=zorder, **whis_kwargs)

    return (l1, l2, l3)


def fancy_legend(color='gray', alpha=.5):
    """

    Usage
    -----
    handle, map = get_fancy_legend()
    ax.legend([handle()], ['legend string'], handler_map={handle: map()})
    """
    # https://stackoverflow.com/questions/39224611/matplotlib-patchcollection-to-legend#39225101
    class AnyObject(object):
        pass

    class AnyObjectHandler(object):
        def legend_artist(self, legend, orig_handle, fontsize, handlebox):
            x0, y0 = handlebox.xdescent, handlebox.ydescent
            width, height = handlebox.width, handlebox.height
            hw = 0.5*width
            hh = 0.5*height
            bw1, bw2 = .15*width, .85*width

            # box
            verts1 = [(x0+bw1, y0),
                      (x0+bw1, y0+height),
                      (x0+bw2, y0+height),
                      (x0+bw2, y0),
                      (x0+bw1, y0)]
            codes1 = [Path.MOVETO,
                      Path.LINETO,
                      Path.LINETO,
                      Path.LINETO,
                      Path.CLOSEPOLY]
            path1 = Path(verts1, codes1)
            patch1 = mpatches.PathPatch(
                path1,
                ec='none',
                facecolor=color,
                alpha=alpha)

            # mean
            verts2 = [((x0+hw), y0),
                      (x0+hw, y0+height),
                      (x0+hw, y0),
                      (x0+hw, y0)]
            codes2 = [Path.MOVETO,
                      Path.LINETO,
                      Path.MOVETO,
                      Path.CLOSEPOLY]
            path2 = Path(verts2, codes2)
            patch2 = mpatches.PathPatch(
                path2,
                ls='solid',
                edgecolor=color,
                facecolor="none")

            # whiskers
            verts3 = [((x0), y0+hh),
                     (x0+bw1, y0+hh),
                      (x0+bw1, y0+hh),
                      (x0+bw1, y0+hh)]
            codes3 = [Path.MOVETO,
                      Path.LINETO,
                      Path.MOVETO,
                      Path.CLOSEPOLY]
            path3 = Path(verts3, codes3)
            patch3 = mpatches.PathPatch(
                path3,
                ls='solid',
                edgecolor=color,
                facecolor="none")

            verts4 = [((x0+bw2), y0+hh),
                      (x0+width, y0+hh),
                      (x0+bw2, y0+hh),
                      (x0+bw2, y0+hh)]
            codes4 = [Path.MOVETO,
                      Path.LINETO,
                      Path.MOVETO,
                      Path.CLOSEPOLY]
            path4 = Path(verts4, codes4)
            patch4 = mpatches.PathPatch(
                path4,
                ls='solid',
                edgecolor=color,
                facecolor="none")

            patch = PatchCollection([patch1, patch2, patch3, patch4],
                                    match_original=True)

            handlebox.add_artist(patch)
            return patch

    return AnyObject, AnyObjectHandler


def hatching(ax, lat, lon, condition, hatch='/////'):
    """Adds a hatching layer to an axis object.

    Parameters
    ----------
    ax : matplotlib.pyplot.axis object
    lat : sequence of float, shape (M,)
    lon : sequence of float, shape (N,)
    condition : sequence of bool, shape (M, N)
        Hatching will be drawn where condition is True.
    hatch : valid hatch string
        Note that multiple equivalent characters will lead to a finer hatching.

    Returns
    -------
    None"""
    if isinstance(lat, xr.core.dataarray.DataArray):
        lat = lat.data
    if isinstance(lon, xr.core.dataarray.DataArray):
        lon = lon.data

    dlat = np.unique(lat[1:] - lat[:-1])
    dlon = np.unique(lon[1:] - lon[:-1])
    assert len(dlat) == 1, 'must be evenly spaced'
    assert len(dlon) == 1, 'must be evenly spaced'
    dxx = dlon[0] / 2
    dyy = dlat[0] / 2
    assert np.shape(condition) == (len(lat), len(lon))
    ii, jj = np.where(condition)
    lat_sel = lat[ii]
    lon_sel = lon[jj]

    patches = [
        Polygon(
            [[xx-dxx, yy+dyy], [xx-dxx, yy-dyy],
             [xx+dxx, yy-dyy], [xx+dxx, yy+dyy]])
        for xx, yy in zip(lon_sel, lat_sel)]
    pp = PatchCollection(patches)
    pp.set_alpha(0.)
    pp.set_hatch(hatch)
    ax.add_collection(pp)


if __name__ == '__main__':
    fig, ax = plt.subplots()
    for ii, color in enumerate(get_color_list()):
        ax.hlines(ii, 0, 1, colors=color, lw=3)
    plt.show()
