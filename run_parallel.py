#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A convenience function for multi-processing.

Bugfix 2018-10-02:
Process ID: "Return the “identity” of an object. This is an integer which is
guaranteed to be unique and constant for this object during its lifetime.
Two objects with non-overlapping lifetimes may have the same id() value."
https://stackoverflow.com/questions/44963206/python-processes-create-same-instance-of-class#44963389

-> update sorter, which fails for long var_lists
-> add checks
"""
import logging
import inspect
import numpy as np
import multiprocessing as mp
from copy import copy

logger = logging.getLogger(__name__)


def get_results(queue, results, pids_running, sorter):
    """Get results from func and do basic consistency checks"""
    return_value = queue.get()
    if isinstance(return_value, int):  # check if func has return value
        pid, result = return_value, None
    elif len(return_value) == 1:
        pid, result = return_value[0], None
    elif len(return_value) == 2:
        pid, result = return_value
    else:
        errmsg = ' '.join([
            'func needs to queue.put(x) where x has to be one of',
            'pid, (pid,), (pid, results) and not {}'.format(return_value)])
        raise ValueError(errmsg)

    if pid not in pids_running.keys():  # more meaningful errmsg than pop(pid)
        errmsg = ' '.join([
            '{} is not a valid pid (see docstring'.format(pid),
            'for an example on how to get the pid)'])
        raise ValueError(errmsg)

    sorter[pids_running.pop(pid)] = len(results)  # fill the sorter
    results += (result,)
    logger.debug('Finished process with ID: {}'.format(pid))
    return results, sorter


def run_parallel(func, var_list, args=None, kwargs=None,
                 nr=min(mp.cpu_count(), 10), sort=True,
                 log_progress=None):
    """A wrapper for functions which support multiprocessing.

    Parameters:
    - func (function): Has to take at least one positional argument (elements
      of 'var_list') and one keyword argument (queue=None).
    - var_list (list): List of values for which 'func' will be called in
      parallel.
    - args (tuple, optional): Positional arguments of func (the first
      positional argument of func has to be a the respective elements of
      var_list and therefore must NOT be given in args!)
    - kwargs (dict, optional): Keyword arguments of func.
    - nr (int, optional): Number of parallel processes to start. If nr=1
      func will be called without multiprocessing (for debugging)
    - sort=True (bool, optional): If True (default) the output will be in the
      same order as var_list. If set to false the output will be unsorted
      (as subprocesses might take different times; might be faster)
    - log_progress=None (int, optional): If not None run_paralell will
      log the given number of progress reports (level=INFO).

    Returns:
    [list of return values of func]

    Example:
    import multiprocessing as mp
    def f(x, queue=None):
        if queue is None:
            return x**2
        else:
            pid = mp.current_process().pid
            queue.put((pid, x**2))

    run_parallel(f, [1, 2, 3])
    > [1, 4, 9]

    Warning:
    If func does not have a queue.put statement run_parallel will not work and
    wait for results forever!"""
    results = ()
    # to avoid problems with multiple calls:
    # https://stackoverflow.com/questions/13195989/default-values-for-function-parameters-in-python
    if args is None:
        args = ()
    if kwargs is None:
        kwargs = {}
    else:
        kwargs = copy(kwargs)

    if nr == 1:  # no multiprocessing
        for idx, var in enumerate(var_list):
            results += (func(var, *args, **kwargs),)
        return list(results)  # (None,...) if func has no return value

    # input consistency checks
    if 'queue' in kwargs:
        errmsg = 'The "queue" kwarg is reserved for run_parallel'
        raise ValueError(errmsg)
    if len(inspect.signature(func).parameters) < 2:
        errmsg = ' '.join([
            '"func" needs to take at least 2 arguments: elements of var_list',
            'and queue'])
        raise ValueError(errmsg)
    if 'queue' not in inspect.signature(func).parameters:
        errmsg = '"func" has to take the "queue" kwarg'
        raise ValueError(errmsg)

    logger.debug('Start multiprocessing with {} processes'.format(nr))

    queue = mp.SimpleQueue()
    kwargs['queue'] = queue
    sorter = np.empty(len(var_list), dtype=int)
    pids_running = {}
    for idx, var in enumerate(var_list):
        if log_progress is not None:  # log progress
            if idx % int(sorter.size / float(log_progress)) == 0:
                logger.info('Progress: {:.0%}'.format(idx / float(sorter.size)))

        process = mp.Process(
            name=idx,
            target=func,
            args=(var,) + args,
            kwargs=kwargs)
        process.start()
        assert process.pid not in pids_running.keys()
        pids_running[process.pid] = idx
        logger.debug('Start process with ID: {}'.format(process.pid))

        if len(pids_running) >= nr:
            logger.debug('Queue full, wait for process to finish')
            results, sorter = get_results(queue, results, pids_running, sorter)

    logger.debug('Wait for remaining processes to finish')
    while len(pids_running) != 0:
        results, sorter = get_results(queue, results, pids_running, sorter)
    if log_progress is not None:
        logger.info('Progress: 100%')
    logger.debug('Finished multiprocessing')

    if sort:
        assert len(set(sorter)) == len(sorter)
        return list(np.array(results)[sorter])
    return list(results)
