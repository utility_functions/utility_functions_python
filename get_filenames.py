#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-10-16 15:38:49 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Contains the Filename class, see docstring for more information.

"""
import os
import re
import glob
import numpy as np
import logging
from functools import partial
from copy import copy

logger = logging.getLogger(__name__)


class Filenames(object):

    def __init__(
            self,
            file_pattern=''.join([
                '{varn}/{varn}_{average}_{model}_{scenario}',
                '_{ensemble}_{grid}.nc']),
            base_path='/net/atmos/data/cmip5-ng'):
        """
        Class to filter out filenames fulfilling given criteria

        Parameters:
        - base_path (str, optional): A valid base path.
        - file_pattern (str, optional): filepath pattern relative to base_path.
          Variables 'x' should be given in the form {x} (will be replaced by
          str.format(x=value)). All variables will be set to '*' by default
          and match all available options (using glob). Variables can appear
          in the string multiple times (e.g., '{x}/{x}_{y}.nc').

        Returns:
        Model object
        """

        self.base_path = base_path
        self.file_pattern = file_pattern
        self.filenames = ()
        self.keys = tuple(re.findall('\{([A-Za-z0-9_]+)\}', file_pattern))
        self.variables = {key: ['*'] for key in set(self.keys)}
        self.indices = None
        self.filters = {}

    def apply_filter(self, force=False, **kwargs):
        """
        Apply filters to restrict 'file_pattern'.

        Parameters:
        - force=False (bool, optional): If True skip consistency tests.
        - **kwargs (dict, optional): Dictionary of {key: value} pairs. 'key'
          has to be a valid variable name as specified in 'file_pattern'.
          'value' has to be one of [None | str | list of str ]. None is equal
          to the '*' string, list will match all elements.
        """
        self.filters.update(kwargs)

        for key, value in kwargs.items():
            if key not in self.variables.keys():
                errmsg = ' '.join([
                    '"{}" is an invalid keyword argument for this',
                    'function']).format(key)
                raise ValueError(errmsg)
            if value is None:
                self.variables[key] = ['*']
            elif isinstance(value, str):
                self.variables[key] = [value]
            elif isinstance(value, list):
                if len(value) != len(set(value)) and not force:
                    errmsg = ' '.join([
                        'A value appeared twice for key {}. Set force=True',
                        'to suppress this error']).format(key)
                    raise NotImplementedError(errmsg)
                if '*' in value and len(value) != 1 and not force:
                    errmsg = ' '.join([
                        '"*" was used together with other values. Set',
                        'force=True to suppress this error'])
                    raise NotImplementedError(errmsg)
                self.variables[key] = value
            else:
                errmsg = ' '.join([
                    'Data type not understood. Use one of [None | str',
                    '| list of str]'])
                raise ValueError(errmsg)

        # construct all possible file patterns from input
        file_pattern = [self.file_pattern.format]
        for key in self.variables.keys():
            file_pattern_new = []
            for fp in file_pattern:
                for arg in self.variables[key]:
                    file_pattern_new.append(partial(fp, **{key: arg}))
            file_pattern = file_pattern_new

        self.filenames = ()
        # get all possible file names
        for filename in file_pattern:
            self.filenames += tuple(glob.glob(os.path.join(
                self.base_path, filename())))

        # fill all variables with regular expression strings
        regex = self.file_pattern.format
        for key in self.variables.keys():
            regex = partial(regex, **{key: '([A-Za-z0-9_-]+)'})

        # get variable values from each filename
        indices = {key: () for key in set(self.keys)}
        for filename in self.filenames:
            values = re.findall(regex(), filename)[0]

            # temp loop to account for keys appearing more than once
            indices_temp = {}
            for key, value in zip(self.keys, values):
                indices_temp[key] = value
            # ---

            for key, value in indices_temp.items():
                indices[key] += (indices_temp[key],)
        self.indices = indices

    def _get_index(self, subset):
        """Get a subset index of a variable."""
        if subset is None:
            return np.ones(len(self.filenames), dtype=bool)

        idx_total = np.ones(len(self.filenames), dtype=bool)
        for key in subset.keys():
            if isinstance(subset[key], str):
                subset[key] = [subset[key]]
            idx = np.zeros(len(self.filenames), dtype=bool)
            for value in subset[key]:
                idx = np.logical_or(idx, value == np.array(self.indices[key]))
            idx_total = np.logical_and(idx_total, idx)
        return idx_total

    def get_filenames(self, subset=None, return_filters=None):
        """
        Returns a list of filenames matching the set filters.

        Parameters:
        - subset=None (dict, optional): Dictionary of {key: value} pairs.
          'key' has to be a valid variable name as specified in 'file_pattern'.
          'value' has to be one or more elements of 'get_variable_values(key)'.
          The connection between values for the same key is 'or' (i.e., key:
          [x, y] will return filenames where key matches either x or y.
          The connection between different keys is 'and' (i.e., key1: x,
          key2: y will return filenames where key1 matches x and key2 matches
          y.
        - return_filters=None ([str | list of str], optional): Also return
          specified variable names for each filename in the form.

        Returns:
        Either (M,) or (M,N) where M is the number of filenames and
        N is len(return_filters)+1
        """
        if self.indices is None:
            errmsg = 'indices not found! Run apply_filter first!'
            logger.error(errmsg)
            raise ValueError(errmsg)
        if subset is not None and not isinstance(subset, dict):
            errmsg = 'subset has to be of type dict and not {}'.format(
                type(subset))
            logger.error(errmsg)
            raise TypeError(errmsg)
        if return_filters is not None and not isinstance(
                return_filters, (list, str)):
            errmsg = ' '.join([
                'return_filters has to be of type list or string',
                'and not {}'.format(type(return_filters))])
            logger.error(errmsg)
            raise TypeError(errmsg)

        idx = self._get_index(subset)
        filenames = np.array(self.filenames)[idx]

        if len(filenames) == 0:
            logger.warning('No files selected')

        if return_filters is None:
            return sorted(filenames)

        indices = {key: np.array(value)[idx]
                   for key, value in self.indices.items()}
        if isinstance(return_filters, str):
            return_filters = [return_filters]
        return sorted(zip(*[indices[filter_]
                          for filter_ in return_filters], filenames))

    def get_variable_values(self, key, subset=None):
        """
        Return all possible values for a given key.

        Parameters:
        - key (str):  A valid variable name as specified in 'file_pattern'.
        - subset=None (dict, optional): Dictionary of {key: value} pairs.
          'key' has to be a valid variable name as specified in 'file_pattern'.
          'value' has to be one or more elements of 'get_variable_values(key)'.
          The connection between values for the same key is 'and' (i.e., key:
          [x, y] will return values where key matches x and y. NOTE: this
          differs from the behavior of get_filenames!
          The connection between different keys is 'and' (i.e., key1: x,
          key2: y will return filenames where key1 matches x and key2 matches
          y.

        Returns:
        - list of str

        Example:
        To get all models with cover RCP2.6 & RCP8.5 as well as tas & pr
        (without knowing which ones those are) do:

        fn = Filenames()
        fn.apply_filter({'scenario': ['rcp26', rcp85'], 'varn': ['tas', 'pr']})
        mn = fn.get_variable_values('model', subset={'scenario': ['rcp26', rcp85'],
                                                     'varn': ['tas', 'pr']})
        filenames = fn.get_filenames(subset={'model': mn})
        """
        if self.indices is None:
            errmsg = 'indices not found! Run apply_filter first!'
            logger.error(errmsg)
            raise ValueError(errmsg)

        if subset is None:
            return sorted(set(self.indices[key]))

        key_values = set(self.indices[key])
        for kk, value in subset.items():
            if isinstance(value, str):
                value = [value]
            for vv in value:
                # create a dummy subclass equivalent to the main class
                # but with additional restrictions
                ff = Filenames(self.file_pattern, self.base_path)
                filters = copy(self.filters)
                filters.update(subset)
                filters.update({kk: vv})
                ff.apply_filter(**filters)
                key_values = key_values.intersection(ff.get_variable_values(key))

        return sorted(key_values)

    def get_variable_keys(self):
        if self.indices is None:
            errmsg = 'indices not found! Run apply_filter first!'
            logger.error(errmsg)
            raise ValueError(errmsg)

        return sorted(set(self.keys))
