#!/bin/bash

for ff in test_*.py; do
    echo "Run $ff..."
    coverage run -p $ff
    echo -e '\n'
done

coverage combine
coverage report -m --rcfile=.coveragerc.ini
