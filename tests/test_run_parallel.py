#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-10-04 14:27:29 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import logging
import unittest
import time
import numpy as np
import multiprocessing as mp

from utils_python.run_parallel import run_parallel


class TestUtils(unittest.TestCase):

    def setUp(self):
        pass

    # @unittest.skip
    def test_multiprocessing(self):

        def f1(x, queue=None):
            time.sleep(.001*(x%5))  # make processes take different times
            if queue is not None:
                pid = mp.current_process().pid
                queue.put((pid, x**2))
            return x**2

        def f2(x, queue=None):
            time.sleep(.001*(x%5))
            if queue is not None:
                pid = mp.current_process().pid
                queue.put((pid, ('{}**2'.format(x), x**2)))
            return ('{}**2'.format(x), x**2)

        # var_list needs to be a list
        self.assertRaises(TypeError, run_parallel, f1, 1)
        # the 'queue' kwarg is reserved for run_parallel and must not be in kwargs
        self.assertRaises(ValueError, run_parallel, f1, [1], kwargs={'queue': None})
        # function must take at least 2 arguments: elements of var_list and queue
        def f(x): pass
        self.assertRaises(ValueError, run_parallel, f, [1])
        # the function f must take the 'queue' kwarg
        def f(x, y): pass
        self.assertRaises(ValueError, run_parallel, f, [1])

        # NOTE: I was not able to catch that case!
        # # the function does not put the results into the queue
        # def f(x, queue=None): return x
        # with self.assertRaises(ValueError):
        #     run_parallel(f, [1, 2])

        # function does not put pid in the queue
        def f(x, queue): queue.put(x)
        self.assertRaises(ValueError, run_parallel, f, [1])
        def f(x, queue): queue.put((x,))
        self.assertRaises(ValueError, run_parallel, f, [1])
        def f(x, queue): queue.put((x, x))
        self.assertRaises(ValueError, run_parallel, f, [1])

        # function puts too many values in the queue
        def f(x, queue):
            pid = mp.current_process().pid
            queue.put((pid, x, x))
        self.assertRaises(ValueError, run_parallel, f, [1])

        # test different call structures
        results_ref = [f1(x) for x in np.arange(100)]
        results_no_multip = run_parallel(f1, np.arange(100), nr=1)
        results_multip = run_parallel(f1, np.arange(100))
        results_multip_unsorted = run_parallel(f1, np.arange(100), sort=False)

        self.assertIsInstance(results_no_multip, list)
        self.assertIsInstance(results_multip, list)
        self.assertIsInstance(results_multip_unsorted, list)

        self.assertListEqual(results_ref, results_no_multip)
        self.assertListEqual(results_ref, results_multip)
        self.assertListEqual(results_ref, sorted(results_multip_unsorted))

        results_ref = [f2(x) for x in np.arange(100)]
        results_no_multip = run_parallel(f2, np.arange(100), nr=1)
        results_multip = run_parallel(f2, np.arange(100))
        results_multip_unsorted = run_parallel(f2, np.arange(100), sort=False)
        np.testing.assert_array_equal(results_ref, results_no_multip)
        np.testing.assert_array_equal(results_ref, results_multip)
        np.testing.assert_array_equal(
            results_ref,
            sorted(results_multip_unsorted, key=lambda x: x[1]))

        def f3(x, y, z, a=None, b=None, c=None, queue=None):
            time.sleep(.001*(x%5))
            if queue is not None:
                pid = mp.current_process().pid
                queue.put((pid, (x, y, z, a, b, c)))
            return (x, y, z, a, b, c)

        # test args & kwargs
        args = (0, 's')
        kwargs = {'a': 1, 'b': 'ss'}
        results_ref = [f3(x, *args, **kwargs) for x in np.arange(100)]
        results_no_multip = run_parallel(f3, np.arange(100), args=args, kwargs=kwargs, nr=1)
        results_multip = run_parallel(f3, np.arange(100), args=args, kwargs=kwargs)
        results_multip_unsorted = run_parallel(f3, np.arange(100), args=args,
                                                     kwargs=kwargs, sort=False)
        self.assertListEqual([0, 0, 's', 1, 'ss', None], list(results_multip[0]))
        np.testing.assert_array_equal(results_ref, results_no_multip)
        np.testing.assert_array_equal(results_ref, results_no_multip)
        np.testing.assert_array_equal(results_ref, results_multip)
        np.testing.assert_array_equal(
            results_ref,
            sorted(results_multip_unsorted, key=lambda x: x[0]))

        args = (0, 's', 1)
        kwargs = {'c': 'ss'}
        results_ref = [f3(x, *args, **kwargs) for x in np.arange(100)]
        results_no_multip = run_parallel(f3, np.arange(100), args=args, kwargs=kwargs, nr=1)
        results_multip = run_parallel(f3, np.arange(100), args=args, kwargs=kwargs)
        results_multip_unsorted = run_parallel(f3, np.arange(100), args=args,
                                                     kwargs=kwargs, sort=False)
        self.assertListEqual([0, 0, 's', 1, None, 'ss'], list(results_multip[0]))
        np.testing.assert_array_equal(results_ref, results_no_multip)
        np.testing.assert_array_equal(results_ref, results_no_multip)
        np.testing.assert_array_equal(results_ref, results_multip)
        np.testing.assert_array_equal(
            results_ref,
            sorted(results_multip_unsorted, key=lambda x: x[0]))

    # @unittest.skip  # NOTE: this test runs a long time
    def test_many_arguments_sorting(self):
        # fixed at 2018-10-02
        def f(x, queue):
            time.sleep(.001*(x%5))
            pid = mp.current_process().pid
            queue.put((pid, x))
        logger = logging.basicConfig(level=logging.INFO)
        results = run_parallel(f, range(50000), sort=True, log_progress=10)
        self.assertListEqual(results, list(range(50000)))


if __name__ == '__main__':
    logging.disable(logging.CRITICAL)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestUtils)
    unittest.TextTestRunner(verbosity=2).run(suite)
