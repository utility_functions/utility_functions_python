#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-09-23 13:37:38 lukas>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import logging
import unittest
import numpy as np

from utils_python.utils import read_config


class TestUtils(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_read_config(self):
        default_ref = dict(
            string='string',
            empty_string='',
            integer=1,
            float1=1.,
            float2=.1,
            bool=True,
            none=None,
            string_list=['string1', 'string2'],
            integer_list=[1, 2],
            float_list=[1., .1],
            mixed_list=['string', 1, .1, True, None],
            one_item_list1=[1.],
            one_item_list2=[True],
            forced_string1='1',
            forced_string2='1.',
            forced_string3='True',
            forced_string4='None',
            forced_string5='string',
            forced_string_list=[1, '1', '1.', 'True', 'None'],
            trailing_comma_list=[1, 2],
            only_in_default='default',
            reset_in_config1='default',
            upper_case_key=''  # NOTE: config changes to lower case
        )
        config_ref = default_ref.copy()
        config_ref.update(dict(
            reset_in_config1='config1',
            not_in_default='config1'))

        default = read_config(path='./config_test.ini')
        config = read_config('config1', './config_test.ini')

        for key in config_ref.keys():
            msg = 'Test failed on key {}'.format(key)
            if isinstance(config_ref[key], list):
                self.assertIsInstance(getattr(config, key), list, msg=msg)
                self.assertIsInstance(getattr(default, key), list, msg=msg)
                self.assertListEqual(getattr(config, key), config_ref[key], msg=msg)
                self.assertListEqual(getattr(default, key), default_ref[key], msg=msg)
            else:
                self.assertNotIsInstance(getattr(config, key), list, msg=msg)
                self.assertEqual(getattr(config, key), config_ref[key], msg=msg)
                if key == 'not_in_default':
                    with self.assertRaises(AttributeError, msg=msg):
                        getattr(default, key)
                else:
                    self.assertNotIsInstance(getattr(default, key), list, msg=msg)
                    self.assertEqual(getattr(default, key), default_ref[key], msg=msg)


if __name__ == '__main__':
    logging.disable(logging.CRITICAL)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestUtils)
    unittest.TextTestRunner(verbosity=2).run(suite)
