#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-09-18 09:59:19 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A template for building a test class.

Links:
General information: https://docs.python.org/3.6/library/unittest.html
Numpy testing: https://docs.scipy.org/doc/numpy/reference/routines.testing.html
setUp and tearDown methods: https://stackoverflow.com/questions/6854658/explain-the-setup-and-teardown-python-methods-used-in-test-cases

"""
import logging
import unittest
import numpy as np

# use this to skip a test (e.g., because it is broken at the moment)
# @unittest.skip(logmsg)

# use this if you expect the test to fail but don't want to skip it
# @unittest.expectedFailure

# some unittest Methods
# Method        	      Checks that             	New in
# assertEqual(a, b) 	      a == b
# assertNotEqual(a, b) 	      a != b
# assertTrue(x) 	      bool(x) is True
# assertFalse(x) 	      bool(x) is False
# assertIs(a, b)              a is b                  	3.1
# assertIsNot(a, b) 	      a is not b                3.1
# assertIsNone(x) 	      x is None                 3.1
# assertIsNotNone(x) 	      x is not None             3.1
# assertIn(a, b) 	      a in b 	                3.1
# assertNotIn(a, b) 	      a not in b                3.1
# assertIsInstance(a, b)      isinstance(a, b)    	3.2
# assertNotIsInstance(a, b)   not isinstance(a, b) 	3.2
# assertMultiLineEqual(a, b)  strings 	                3.1
# assertSequenceEqual(a, b)   sequences 	        3.1
# assertListEqual(a, b)       lists 	                3.1
# assertTupleEqual(a, b)      tuples 	                3.1
# assertSetEqual(a, b) 	      sets or frozensets 	3.1
# assertDictEqual(a, b)       dicts 	                3.1

# For array tests see numpy.testing

class TestFunction(unittest.TestCase):

    def setUp(self):
        # setUp and tearDown will be run before each test and
        pass

    def tearDown(self):
        pass

    def test_function(self):
        pass


if __name__ == '__main__':
    logging.disable(logging.NOTSET)  # disable logging
    suite = unittest.TestLoader().loadTestsFromTestCase(TestUtils)
    unittest.TextTestRunner(verbosity=2).run(suite)
