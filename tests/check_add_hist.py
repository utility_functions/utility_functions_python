#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-09-14 11:04:25 lukas>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Super basic test for add_hist.

"""
import xarray as xr

from utils_python.xarray import add_hist

ds = xr.Dataset()
add_hist(ds)
print(ds.attrs['history'])
