#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-10-04 12:02:15 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import math
import logging
import unittest
import numpy as np

from utils_python.physics import (
    area_weighted_mean,
    haversine_grid,
    AVG_EARTH_RADIUS)


class TestPhysics(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_area_weighted_mean(self):

        time = np.arange(5)
        lat = np.arange(-88.75, 90., 2.5)
        lon1 = np.arange(1.25, 360, 2.5)
        lon2 = np.arange(-180, 180, 2.5)
        data1 = np.ones(time.shape + lat.shape + lon1.shape)
        data2 = np.ones(lat.shape + lon1.shape)
        data3 = np.ones(lat.shape)

        self.assertRaises(AssertionError, area_weighted_mean, data1, lat, 0)
        self.assertRaises(AssertionError, area_weighted_mean, data1, 0, lon1)
        self.assertRaises(AssertionError, area_weighted_mean, data1, lat*10, lon1)
        self.assertRaises(AssertionError, area_weighted_mean, data1, lat, lon1*10)
        self.assertRaises(AssertionError, area_weighted_mean, data1, lon1, lat)
        self.assertRaises(AssertionError, area_weighted_mean, data1, lat, lat)
        self.assertRaises(AssertionError, area_weighted_mean, data1, lon1, lon1)

        mean1 = area_weighted_mean(data1, lat, lon1)
        mean2 = area_weighted_mean(data2, lat, lon1)
        mean3 = area_weighted_mean(data3, lat, None)
        mean4 = area_weighted_mean(data1.swapaxes(-1, -2), lat, None)

        self.assertTrue(mean1.shape==time.shape)
        self.assertTrue(mean4.shape==time.shape+lon1.shape)

        self.assertListEqual([1.] * data1.shape[0], list(np.around(mean1, 7)))
        self.assertAlmostEqual(1., mean2)
        self.assertAlmostEqual(1., mean3)

        mean1 = area_weighted_mean(data1, lat, lon2)
        mean2 = area_weighted_mean(data2, lat, lon2)
        self.assertTrue(mean1.shape==time.shape)
        self.assertListEqual([1.] * data1.shape[0], list(np.around(mean1, 7)))
        self.assertAlmostEqual(1., mean2)

        np.random.seed(0)
        data4 = np.random.randint(1000, size=time.shape + lat.shape + lon1.shape)
        data4_ref = np.around(np.mean(np.average(data4, axis=1, weights=np.cos(np.radians(lat))), axis=1), 7)
        mean4 = area_weighted_mean(data4, lat, lon1)
        self.assertListEqual(list(data4_ref), list(np.around(mean4, 7)))

        # test weighted mean with NANs
        data1 = np.ones(time.shape + lat.shape + lon1.shape)
        data2 = np.ones(time.shape + lat.shape + lon1.shape)
        data3 = np.random.randint(1000, size=time.shape + lat.shape + lon1.shape) * 1.
        data2[0, 0, :] = np.nan
        data2[1, :, :] = np.nan
        data3[0, :, 0] = np.nan
        data3[0, 0, :] = np.nan
        data3[1, :, :] = np.nan
        data3[2, :, :] = np.nan
        data3[2, 0, 0] = 1.
        data3[2, -1, 0] = 2.
        data3[2, -1, -1] = 2.
        data3 = np.ma.masked_invalid(data3)
        weights = np.tile(np.cos(np.radians(lat)), (len(lon1), 1)).swapaxes(0, 1).ravel()
        data3_ref = np.ma.around(np.ma.average(
            data3.reshape(5, -1),
            axis=-1,
            weights=weights), 7)

        mean1 = area_weighted_mean(data1, lat, lon1)
        mean2 = area_weighted_mean(data2, lat, lon1)
        mean3 = area_weighted_mean(data3, lat, lon1)
        self.assertListEqual([1.] * data1.shape[0], list(np.around(mean1, 7)))
        self.assertAlmostEqual(1., mean2[0])
        self.assertTrue(np.isnan(mean2[1]))
        self.assertAlmostEqual(1.66666667, mean3[2])
        np.testing.assert_array_almost_equal(data3_ref, mean3)


    def test_area_weighted_mean_ncl(self):

        def area_avg(data_3D, lat, lon):
            '''
            Calculate area average for each model in data. Based on NCL.
            Converted by R. Lorenz.

            Parameters:
            - data (np.array): Shape(M, time, lat, lon) or (M, lat, lon)
            - lat (np.array): the latitudes
            - lon (np.array): the longitudes

            Returns:
            - np.array shape = (M,)
            '''
            rank = len(data_3D.shape)
            if isinstance(data_3D, np.ma.core.MaskedArray):
                data_nan = data_3D.filled(np.nan)
            else:
                data_nan = data_3D
            rad = 4.0 * math.atan(1.0) / 180
            w_lat = np.cos(lat * rad) # weight for latitude differences in area
            if rank == 4:
                sumx = np.zeros((data_3D.shape[0], data_3D.shape[1]))
                sumw = 0.0
                for ilat in range(len(lat)):
                    for ilon in range(len(lon)):
                        if not np.isnan(data_nan[:, :, ilat, ilon]).all():  # NOTE: this will give nan if there is any nan in the first two dimensions!!
                            sumx = sumx + data_nan[:, :, ilat, ilon] * w_lat[ilat]
                            sumw = sumw + w_lat[ilat]
                data_areaavg_loop = sumx / sumw
            elif rank == 3:
                sumx = np.zeros((data_3D.shape[0]))
                sumw = 0.0
                for ilat in range(len(lat)):
                    for ilon in range(len(lon)):
                        if not np.isnan(data_nan[:, ilat, ilon]).all():
                            sumx = sumx + data_nan[:, ilat, ilon] * w_lat[ilat]
                            sumw = sumw + w_lat[ilat]
                data_areaavg_loop = sumx / sumw

            return data_areaavg_loop

        time = np.arange(5)
        lat = np.arange(-88.75, 90., 2.5)
        lon = np.arange(1.25, 360, 2.5)
        np.random.seed(616814)
        data = np.random.rand(time.size, lat.size, lon.size)
        mean = area_weighted_mean(data, lat, lon)
        mean_ref = area_avg(data, lat, lon)
        np.testing.assert_array_almost_equal(mean, mean_ref)

    def test_haversine_grid(self):

        radius = AVG_EARTH_RADIUS

        lat = np.arange(-80, 90, 20)
        lon1 = np.arange(0, 360, 60)
        lon2 = np.arange(-180, 180, 60)

        # distance should be zero at the point
        point_ref = (0, 0)
        dd1 = haversine_grid(point_ref, lat, lon1)
        dd2 = haversine_grid(point_ref, lat, lon2)
        i_lat = np.where(lat==point_ref[0])[0]
        i_lon1 = np.where(lon1==point_ref[1])[0]
        i_lon2 = np.where(lon2==point_ref[1])[0]
        self.assertEqual(0., dd1[i_lat, i_lon1])
        self.assertEqual(0., dd2[i_lat, i_lon2])

        # some other know distances:
        # other side of the earth
        i_lat = np.where(lat==0)[0]
        i_lon1 = np.where(lon1==180)[0]
        i_lon2 = np.where(lon2==-180)[0]
        self.assertEqual(np.around(radius*np.pi, 7), dd1[i_lat, i_lon1])
        self.assertEqual(np.around(radius*np.pi, 7), dd2[i_lat, i_lon2])

        point_ref = (-20, 60)
        dd1 = haversine_grid(point_ref, lat, lon1)
        dd2 = haversine_grid(point_ref, lat, lon2)
        i_lat = np.where(lat==20)[0]
        i_lon1 = np.where(lon1==240)[0]
        i_lon2 = np.where(lon2==-120)[0]
        self.assertEqual(np.around(radius*np.pi, 7), dd1[i_lat, i_lon1])
        self.assertEqual(np.around(radius*np.pi, 7), dd2[i_lat, i_lon2])

        # if lat=-90/90 distance should not depend on longitude
        point_ref = (90, 0)
        dd1 = haversine_grid(point_ref, lat, lon1)
        dd2 = haversine_grid(point_ref, lat, lon2)
        self.assertTrue(np.unique(dd1, axis=1).squeeze().shape==lat.shape)
        self.assertTrue(np.unique(dd2, axis=1).squeeze().shape==lat.shape)

        # largest possible distance should be r*pi
        lat = np.arange(-89.5, 90., 1.)
        lon1 = np.arange(0, 360, 1.)
        lon2 = np.arange(-180., 180., 1.)
        dd1 = haversine_grid(point_ref, lat, lon1)
        dd2 = haversine_grid(point_ref, lat, lon2)
        self.assertTrue(dd1.max()<=radius*np.pi)
        self.assertTrue(dd2.max()<=radius*np.pi)


if __name__ == '__main__':
    logging.disable(logging.CRITICAL)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPhysics)
    unittest.TextTestRunner(verbosity=2).run(suite)
