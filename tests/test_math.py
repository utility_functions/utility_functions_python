#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-10-03 15:05:13 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import math
import logging
import unittest
import numpy as np

from utils_python.math import variance, quantile


class TestMath(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass


    def test_variance(self):

        # compare unweighted cases
        np.random.seed(986)
        data = np.random.rand(1000)

        self.assertEqual(variance(data), np.var(data, ddof=1))
        self.assertEqual(variance(data, biased=True), np.var(data))

        idx = np.random.randint(1000, size=100)
        data[idx] = np.nan
        self.assertEqual(variance(data), np.nanvar(data, ddof=1))
        self.assertEqual(variance(data, biased=True), np.nanvar(data))

        data = np.ma.masked_invalid(data)
        self.assertEqual(variance(data), np.ma.var(data, ddof=1))
        self.assertEqual(variance(data, biased=True), np.ma.var(data))

        # sanity test: large enough array with random weights should lead to
        # a similar results as unweighted case
        data = np.random.rand(1000)
        weights = np.random.rand(1000)
        self.assertAlmostEqual(variance(data, weights=weights), np.var(data, ddof=1), places=3)
        self.assertAlmostEqual(variance(data, weights=weights, biased=True), np.var(data), places=3)
        data[idx] = np.nan
        self.assertAlmostEqual(variance(data), np.nanvar(data, ddof=1), places=3)
        self.assertAlmostEqual(variance(data, biased=True), np.nanvar(data), places=3)
        data = np.ma.masked_invalid(data)
        self.assertAlmostEqual(variance(data), np.ma.var(data, ddof=1), places=3)
        self.assertAlmostEqual(variance(data, biased=True), np.ma.var(data), places=3)

        # large random dataset
        # create reference by repeating elements
        data = np.random.rand(1000)
        data_ref = data
        data_ref[:100] = data[0]
        data_ref[100:150] = data[100]
        data_ref[150:170] = data[150]
        data_ref[170:172] = data[170]
        data_ref[172:174] = data[172]
        data_ref[174:176] = data[174]
        # create data & weights accordingly
        data = np.concatenate(([data[0]], [data[100]], [data[150]], [data[170]],
                               [data[172]], [data[174]], data[176:]))
        weights = np.ones_like(data)
        weights[0] = 100
        weights[1] = 50
        weights[2] = 20
        weights[3] = 2
        weights[4] = 2
        weights[5] = 2
        # NOTE: the unbiased results are less equal, presumably because the number of elements differs!
        self.assertAlmostEqual(variance(data, weights=weights), np.var(data_ref, ddof=1), places=2)
        self.assertAlmostEqual(variance(data, weights=weights, biased=True), np.var(data_ref), places=8)
        idx = np.random.randint(176, 1000, size=100)
        data_ref[idx] = np.nan
        data = np.concatenate(([data_ref[0]], [data_ref[100]], [data_ref[150]], [data_ref[170]],
                               [data_ref[172]], [data_ref[174]], data_ref[176:]))
        self.assertAlmostEqual(variance(data, weights=weights), np.nanvar(data_ref, ddof=1), places=2)
        self.assertAlmostEqual(variance(data, weights=weights, biased=True), np.nanvar(data_ref), places=8)
        data = np.ma.masked_invalid(data)
        data_ref = np.ma.masked_invalid(data_ref)
        self.assertAlmostEqual(variance(data, weights=weights), np.ma.var(data_ref, ddof=1), places=2)
        self.assertAlmostEqual(variance(data, weights=weights, biased=True), np.ma.var(data_ref), places=8)

        # test invalid value handling
        data = np.empty(10) * np.nan
        weights = np.ones_like(data)
        np.testing.assert_equal(variance(data), np.nan)
        np.testing.assert_equal(variance(data, weights=weights), np.nan)
        np.testing.assert_equal(variance(data, weights=weights, biased=True), np.nan)
        np.testing.assert_equal(variance(data, biased=True), np.nan)
        data = np.ma.masked_invalid(data)
        self.assertIsInstance(variance(data), np.ma.core.MaskedArray)
        self.assertTrue(variance(data).mask)
        self.assertTrue(variance(data, weights=weights).mask)
        self.assertTrue(variance(data, weights=weights, biased=True).mask)
        self.assertTrue(variance(data, biased=True).mask)


    def test_quantile(self):
        data = np.arange(101)

        self.assertRaises(ValueError, quantile, data, quantiles=1.1)  # quantiles != [0, 1]
        self.assertRaises(ValueError, quantile, data, quantiles=-.1)  # quantiles != [0, 1]
        self.assertRaises(ValueError, quantile, data, quantiles=[.9, 1.1])  # quantiles != [0, 1]
        self.assertRaises(ValueError, quantile, data, quantiles=[-.1, .9])  # quantiles != [0, 1]
        self.assertRaises(ValueError, quantile, data, .9, weights=data[1:])  # data.shape != weights.shape
        self.assertRaises(ValueError, quantile, 1, .9)  # data != array

        # compare unweighted cases
        for interpolation in ['linear', 'lower', 'higher', 'nearest']:
            self.assertAlmostEqual(
                quantile(data, .789, interpolation=interpolation, old_style=True),
                np.percentile(data, 78.9, interpolation=interpolation),
                places=6, msg=interpolation)
            np.testing.assert_array_almost_equal(
                quantile(data, (0., .2, .333, .5, .99, 1.),
                         interpolation=interpolation, old_style=True),
                np.percentile(data, (0, 20, 33.3, 50, 99, 100),
                              interpolation=interpolation),
                decimal=6, err_msg=interpolation)

        # compare weighted cases
        data = np.arange(11)
        weights = [1, 1, 2, 3, 4, 5, 4, 3, 2, 1, 1]
        data_ref = np.array([0, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5,
                             6, 6, 6, 6, 7, 7, 7, 8, 8, 9, 10])
        for interpolation in ['linear', 'nearest']:
            self.assertAlmostEqual(
                quantile(data, .789, weights=weights,
                         interpolation=interpolation, old_style=True),
                np.percentile(data_ref, 78.9, interpolation=interpolation),
                places=0, msg=interpolation)
            np.testing.assert_array_almost_equal(
                quantile(data, (0., .2, .333, .5, .99, 1.), weights=weights,
                         interpolation=interpolation, old_style=True),
                np.percentile(data_ref, (0, 20, 33.3, 50, 99, 100),
                              interpolation=interpolation),
                decimal=1, err_msg=interpolation)

        # sanity test: large enough array with random weights should lead to
        # a similar results as unweighted case
        np.random.seed(986)
        data = np.random.rand(10000)
        weights = np.random.rand(10000)
        for interpolation in ['linear', 'lower', 'higher', 'nearest']:
            np.testing.assert_array_almost_equal(
                quantile(data, (0., .2, .333, .5, .99, 1.), weights=weights,
                         interpolation=interpolation, old_style=True),
                np.percentile(data, (0, 20, 33.3, 50, 99, 100),
                              interpolation=interpolation),
                decimal=2, err_msg=interpolation)

        # large random dataset
        # create reference by repeating elements
        data_ref = data
        data_ref[:100] = data[0]
        data_ref[100:150] = data[100]
        data_ref[150:170] = data[150]
        data_ref[170:172] = data[170]
        data_ref[172:174] = data[172]
        data_ref[174:176] = data[174]
        # create data & weights accordingly
        data = np.concatenate(([data[0]], [data[100]], [data[150]], [data[170]],
                               [data[172]], [data[174]], data[176:]))
        weights = np.ones_like(data)
        weights[0] = 100
        weights[1] = 50
        weights[2] = 20
        weights[3] = 2
        weights[4] = 2
        weights[5] = 2
        for interpolation in ['linear', 'lower', 'higher', 'nearest']:
            np.testing.assert_array_almost_equal(
                quantile(data, (0., .2, .333, .5, .99, 1.), weights=weights,
                         interpolation=interpolation, old_style=True),
                np.percentile(data_ref, (0, 20, 33.3, 50, 99, 100),
                              interpolation=interpolation),
                decimal=8, err_msg=interpolation)

        # this should also work with non-integer weights (to a certain
        # accuracy
        weights[0] = 100.1
        weights[1] = 49.5
        weights[2] = 20.2
        weights[3] = 2.01
        weights[4] = 2.
        weights[5] = 2.
        for interpolation in ['linear', 'lower', 'higher', 'nearest']:
            np.testing.assert_array_almost_equal(
                quantile(data, (0., .2, .333, .5, .99, 1.), weights=weights,
                         interpolation=interpolation, old_style=True),
                np.percentile(data_ref, (0, 20, 33.3, 50, 99, 100),
                              interpolation=interpolation),
                decimal=3, err_msg=interpolation)


    @unittest.expectedFailure
    def test_quantile_interpolation(self):
        # compare weighted cases
        data = np.arange(11)
        weights = [1, 1, 2, 3, 4, 5, 4, 3, 2, 1, 1]
        data_ref = np.array([0, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5,
                             6, 6, 6, 6, 7, 7, 7, 8, 8, 9, 10])
        # NOTE: the 'lower' & 'higher' kwargs are implemented in accordance with np.percentile
        # but the behavior is not always intuitive so they should be used with care!
        # This is particularly true for integer data & weights. In this case it might be better
        # to weight 'manually' by repeating elements.
        for interpolation in ['lower', 'higher']:
            self.assertAlmostEqual(
                quantile(data, .789, weights=weights,
                         interpolation=interpolation, old_style=True),
                np.percentile(data_ref, 78.9, interpolation=interpolation),
                places=0, msg=interpolation)
            np.testing.assert_array_almost_equal(
                quantile(data, (0., .2, .333, .5, .99, 1.), weights=weights,
                         interpolation=interpolation, old_style=True),
                np.percentile(data_ref, (0, 20, 33.3, 50, 99, 100),
                              interpolation=interpolation),
                decimal=1, err_msg=interpolation)



if __name__ == '__main__':
    logging.disable(logging.CRITICAL)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestMath)
    unittest.TextTestRunner(verbosity=2).run(suite)
