#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-09-19 14:31:19 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""

import unittest
import xarray as xr

from utils_python.get_filenames import Filenames


class TestModels(unittest.TestCase):

    def setUp(self):
        pass

    def test_input(self):

        fn = Filenames(
            base_path='/net/atmos/data/cmip5-ng',
            file_pattern='{varn}/{varn}_ann_{model}_rcp26_r1i1p1_g025{varn}XX.nc')

        # input handling
        self.assertEqual(fn.keys, ('varn', 'varn', 'model', 'varn'))
        self.assertEqual(sorted(fn.variables.keys()), sorted(['varn', 'model']))
        self.assertEqual(sorted(fn.variables.values()), sorted([['*'], ['*']]))

        ## filter handling
        # (i) empty input -> values should stay unchanged
        fn.apply_filter()
        fn.apply_filter(**{'varn': None})
        fn.apply_filter(**{'varn': None, 'model': None})
        fn.apply_filter(**{'varn': '*'})
        fn.apply_filter(**{'varn': None, 'model': '*'})
        fn.apply_filter(**{'varn': ['*']})
        fn.apply_filter(**{'varn': '*', 'model': ['*']})
        self.assertEqual(sorted(fn.variables.values()), sorted([['*'], ['*']]))
        self.assertEqual(fn.filenames, ())  # dummy input no files found
        self.assertEqual(fn.indices, {'varn': (), 'model': ()})

        # (ii) valid input
        fn.apply_filter(**{'varn': 'tas'})
        fn.apply_filter() # should do nothing
        self.assertEqual(sorted(fn.variables.values()), sorted([['tas'], ['*']]))
        fn.apply_filter(**{'varn': 'tas', 'model': 'CanESM2'})
        self.assertEqual(sorted(fn.variables.values()), sorted([['tas'], ['CanESM2']]))
        fn.apply_filter(**{'varn': None, 'model': '*'})  # reset
        self.assertEqual(sorted(fn.variables.values()), sorted([['*'], ['*']]))

        # (iii) invalid input
        self.assertRaises(ValueError, fn.apply_filter, **{'XX': 'xx'})  # key unknown
        self.assertRaises(ValueError, fn.apply_filter, **{'varn': 1})  # type(value) wrong
        self.assertRaises(NotImplementedError, fn.apply_filter,
                          **{'varn': ['tas', 'tas']})  # value appears twice
        self.assertRaises(NotImplementedError, fn.apply_filter,
                          **{'varn': ['*', 'tas']})  # wildcard + value mix

    def test_output_basic(self):

        fn = Filenames(
            base_path='/net/atmos/data/cmip5-ng',
            file_pattern='tas/tas_ann_{model_name}_{scenario_name}_r1i1p1_g025.nc')

        fn.apply_filter(**{'scenario_name': ['rcp26', 'rcp85']})
        self.assertEqual(sorted(list(fn.indices.keys())),
                         sorted(['model_name', 'scenario_name']))
        self.assertEqual(sorted(set(fn.indices['scenario_name'])),
                         sorted(['rcp26', 'rcp85']))
        # self.assertEqual(sorted(set(fn.indices['model_name'])),
        #                  sorted([]))  # omitted
        self.assertEqual(fn.get_variable_keys(),
                         sorted(['model_name', 'scenario_name']))
        self.assertEqual(fn.get_variable_values('scenario_name'),
                         sorted(['rcp26', 'rcp85']))
        # self.assertEqual(sorted(fn.get_variable_values('model_name')),
        #                  sorted([]))  # omitted

    def test_get_variable_values(self):
        # add this to test a bug in get_variable_values
        # TODO: I think I fixed it, but run additional tests!!

        scenarios = ['rcp26', 'rcp45']
        models = ['EC-EARTH', 'CanESM2']
        fn = Filenames('tas/tas_sea_{model}_{scenario}_{ensemble}_g025.nc')
        fn.apply_filter(scenario=scenarios, model=models)
        self.assertEqual(fn.get_variable_values(
            'ensemble', subset={'model': 'EC-EARTH', 'scenario': 'rcp26'}),
                         ['r12i1p1', 'r8i1p1'])

    def test_output_manual(self):

        fn = Filenames(
            base_path='/net/atmos/data/cmip5-ng',
            file_pattern='tas/tas_ann_{model_name}_{scenario_name}_r1i1p1_g025.nc')
        fn.apply_filter(**{'scenario_name': ['rcp26', 'rcp85']})

        # get filenames from folder:
        # ls -1 /net/atmos/data/cmip5-ng/tas/tas_ann_*_rcp26_r1i1p1_g025.nc | sed -e 's/^/"/' -e 's/$/",/'
        # ls -1 /net/atmos/data/cmip5-ng/tas/tas_ann_*_rcp85_r1i1p1_g025.nc | sed -e 's/^/"/' -e 's/$/",/'
        fn_test_26 = [
            "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_BNU-ESM_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CanESM2_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CCSM4_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CESM1-CAM5_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CNRM-CM5_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CSIRO-Mk3-6-0_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_FGOALS-g2_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_FIO-ESM_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GFDL-CM3_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GFDL-ESM2G_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GFDL-ESM2M_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GISS-E2-H_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GISS-E2-R_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_HadGEM2-AO_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_HadGEM2-ES_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_IPSL-CM5A-LR_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_IPSL-CM5A-MR_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MIROC5_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MIROC-ESM-CHEM_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MIROC-ESM_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MPI-ESM-LR_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MPI-ESM-MR_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MRI-CGCM3_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_NorESM1-ME_rcp26_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_NorESM1-M_rcp26_r1i1p1_g025.nc"]
        fn_test_85 = [
            "/net/atmos/data/cmip5-ng/tas/tas_ann_ACCESS1-0_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_ACCESS1-3_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_BNU-ESM_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CanESM2_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CCSM4_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CESM1-BGC_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CESM1-CAM5_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CMCC-CESM_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CMCC-CM_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CMCC-CMS_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CNRM-CM5_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_CSIRO-Mk3-6-0_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_EC-EARTH_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_FGOALS-g2_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_FIO-ESM_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GFDL-CM3_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GFDL-ESM2G_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GFDL-ESM2M_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GISS-E2-H-CC_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GISS-E2-H_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GISS-E2-R-CC_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_GISS-E2-R_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_HadGEM2-AO_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_HadGEM2-CC_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_HadGEM2-ES_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_inmcm4_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_IPSL-CM5A-LR_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_IPSL-CM5A-MR_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_IPSL-CM5B-LR_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MIROC5_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MIROC-ESM-CHEM_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MIROC-ESM_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MPI-ESM-LR_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MPI-ESM-MR_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MRI-CGCM3_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_MRI-ESM1_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_NorESM1-ME_rcp85_r1i1p1_g025.nc",
            "/net/atmos/data/cmip5-ng/tas/tas_ann_NorESM1-M_rcp85_r1i1p1_g025.nc"]

        mn_test_26 = [ "bcc-csm1-1-m", "bcc-csm1-1", "BNU-ESM",
                       "CanESM2", "CCSM4", "CESM1-CAM5", "CNRM-CM5",
                       "CSIRO-Mk3-6-0", "FGOALS-g2", "FIO-ESM", "GFDL-CM3",
                       "GFDL-ESM2G", "GFDL-ESM2M", "GISS-E2-H", "GISS-E2-R",
                       "HadGEM2-AO", "HadGEM2-ES", "IPSL-CM5A-LR",
                       "IPSL-CM5A-MR", "MIROC5", "MIROC-ESM-CHEM", "MIROC-ESM",
                       "MPI-ESM-LR", "MPI-ESM-MR", "MRI-CGCM3", "NorESM1-ME",
                       "NorESM1-M"]
        mn_test_85 = [ "ACCESS1-0", "ACCESS1-3", "bcc-csm1-1-m",
                       "bcc-csm1-1", "BNU-ESM", "CanESM2", "CCSM4", "CESM1-BGC",
                       "CESM1-CAM5", "CMCC-CESM", "CMCC-CM", "CMCC-CMS",
                       "CNRM-CM5", "CSIRO-Mk3-6-0", "EC-EARTH", "FGOALS-g2",
                       "FIO-ESM", "GFDL-CM3", "GFDL-ESM2G", "GFDL-ESM2M",
                       "GISS-E2-H-CC", "GISS-E2-H", "GISS-E2-R-CC", "GISS-E2-R",
                       "HadGEM2-AO", "HadGEM2-CC", "HadGEM2-ES", "inmcm4",
                       "IPSL-CM5A-LR", "IPSL-CM5A-MR", "IPSL-CM5B-LR", "MIROC5",
                       "MIROC-ESM-CHEM", "MIROC-ESM", "MPI-ESM-LR", "MPI-ESM-MR",
                       "MRI-CGCM3", "MRI-ESM1", "NorESM1-ME", "NorESM1-M"]

        # all
        self.assertEqual(sorted(fn.get_filenames()), sorted(fn_test_26 + fn_test_85))
        # RCP2.6 / RCP85 only
        self.assertEqual(sorted(fn.get_filenames(subset={'scenario_name': 'rcp26'})),
                         sorted(fn_test_26))
        self.assertEqual(sorted(fn.get_filenames(subset={'scenario_name': 'rcp85'})),
                         sorted(fn_test_85))
        # 1 model only
        self.assertEqual(
            sorted(fn.get_filenames(subset={'model_name': 'bcc-csm1-1-m'})),
            sorted([
                "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp26_r1i1p1_g025.nc",
                "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp85_r1i1p1_g025.nc"]))
        # 2 models only (second one has no RCP2.6)
        self.assertEqual(
            fn.get_filenames(subset={'model_name': ['bcc-csm1-1-m', 'ACCESS1-0']}),
            sorted([
                "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp26_r1i1p1_g025.nc",
                "/net/atmos/data/cmip5-ng/tas/tas_ann_ACCESS1-0_rcp85_r1i1p1_g025.nc",
                "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp85_r1i1p1_g025.nc"]))
        # 2 models & RCP8.5 only
        self.assertEqual(
            fn.get_filenames(subset={
                'model_name': ['bcc-csm1-1-m', 'ACCESS1-0'],
                'scenario_name': 'rcp85'}),
            sorted([
                "/net/atmos/data/cmip5-ng/tas/tas_ann_ACCESS1-0_rcp85_r1i1p1_g025.nc",
                "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp85_r1i1p1_g025.nc"]))
        self.assertEqual(
            fn.get_filenames(subset={
                'model_name': ['bcc-csm1-1-m', 'ACCESS1-0'],
                'scenario_name': 'rcp85'}, return_filters='model_name'),
            sorted([
                ('ACCESS1-0', "/net/atmos/data/cmip5-ng/tas/tas_ann_ACCESS1-0_rcp85_r1i1p1_g025.nc"),
                ('bcc-csm1-1-m', "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp85_r1i1p1_g025.nc")]))
        self.assertEqual(
            fn.get_filenames(subset={
                'model_name': ['bcc-csm1-1-m', 'ACCESS1-0'],
                'scenario_name': 'rcp85'}, return_filters=['model_name', 'scenario_name']),
            [('ACCESS1-0', 'rcp85',
              "/net/atmos/data/cmip5-ng/tas/tas_ann_ACCESS1-0_rcp85_r1i1p1_g025.nc"),
             ('bcc-csm1-1-m', 'rcp85',
              "/net/atmos/data/cmip5-ng/tas/tas_ann_bcc-csm1-1-m_rcp85_r1i1p1_g025.nc")])

        self.assertEqual(fn.get_variable_values('model_name'),
                         sorted(set(mn_test_26).union(mn_test_85)))
        self.assertEqual(fn.get_variable_values('model_name', subset={'scenario_name': 'rcp26'}),
                         sorted(mn_test_26))
        self.assertEqual(fn.get_variable_values('model_name', subset={'scenario_name': ['rcp26', 'rcp85']}),
                         sorted(set(mn_test_26).intersection(mn_test_85)))

    def test_content(self):
        fn = Filenames()
        fn.apply_filter(varn=['tas', 'pr'],
                        average='ann',
                        scenario=['rcp26', 'rcp65', 'rcp85'],
                        ensemble='r1i1p1',
                        grid='g025')
        for vv, aa, mm, ss, ee, gg, fn in fn.get_filenames(
                return_filters=['varn', 'average', 'model',
                                'scenario', 'ensemble', 'grid']):
            ds = xr.open_dataset(fn)
            # NOTE: some model names differ from filename to attribute!
            self.assertTrue(((ds.attrs['source_model'] == mm or
                              str(ds.attrs['source_model']) == mm.replace('-', '.') or
                              str(ds.attrs['source_model']) == mm.replace('-', '_'))))
            self.assertEqual(ds.attrs['source_ensemble'], ee)
            self.assertEqual(ds.attrs['source_experiment'], 'historical,'+ss)
            self.assertEqual(ds.attrs['freq'], 'annual')
            self.assertEqual(ds.attrs['interpolation_grid'],
                             '2.5 x 2.5 degrees bilinear interpolation')
            self.assertEqual(list(set(ds.variables).difference(ds.dims))[0], vv)


    def test_bug_multiple_apply_calls(self):
        fn = Filenames()
        fn.apply_filter(varn=['tas', 'pr'],
                        average='ann',
                        scenario=['rcp26', 'rcp65', 'rcp85'],
                        grid='g025')
        ensembles = fn.get_variable_values('ensemble', subset={'model': 'MIROC5'})
        models = fn.get_variable_values('model')
        models.remove('EC-EARTH')
        fn.apply_filter(model=models)
        self.assertListEqual(fn.get_variable_values('ensemble', subset={'model': 'MIROC5'}),
                             ensembles)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestModels)
    unittest.TextTestRunner(verbosity=2).run(suite)
