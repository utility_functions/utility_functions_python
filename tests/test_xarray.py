#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-10-25 15:51:10 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import logging
import unittest
import cftime
import numpy as np
import xarray as xr

from utils_python.xarray import (
    area_weighted_mean,
    flip_antimeridian,
    standardize_time)


class TestUtils(unittest.TestCase):

    def setUp(self):
        pass

    # @unittest.skip
    def test_area_weighted_mean(self):

        time = np.arange(5)
        lat = np.arange(-88.75, 90., 2.5)
        lon = np.arange(1.25, 360, 2.5)
        data1 = np.ones(time.shape + lat.shape + lon.shape)
        data2 = np.ones(lat.shape + lon.shape)

        ds1 = xr.Dataset(data_vars={'data': (('time', 'lat', 'lon'), data1)},
                         coords={'time': time,
                                 'lat': ('lat', lat, {'units': 'degree_north'}),
                                 'lon': lon})
        lon = np.arange(-180, 180, 2.5)
        ds2 = xr.Dataset(data_vars={'data': (('latitude', 'lon'), data2)},
                         coords={'latitude': ('latitude', lat, {'units': 'degrees_north'}),
                                 'lon': lon})

        ds1_global = area_weighted_mean(ds1, lonn='lon')
        ds2_global = area_weighted_mean(ds2, lonn='lon')
        self.assertListEqual([1.] * ds1.dims['time'], list(np.around(ds1_global['data'].data, 7)))
        self.assertAlmostEqual(1., ds2_global['data'].data)

        np.random.seed(0)
        data4 = np.random.randint(1000, size=time.shape + lat.shape + lon.shape)
        ds4 = xr.Dataset(data_vars={'data': (('time', 'lat', 'lon'), data4)},
                         coords={'time': time,
                                 'lat': ('lat', lat, {'units': 'degree_north'}),
                                 'lon': lon})
        data4_ref = np.around(np.mean(np.average(data4, axis=1, weights=np.cos(np.radians(lat))), axis=1), 7)
        ds4_global = area_weighted_mean(ds4, lonn='lon')
        self.assertListEqual(list(data4_ref), list(np.around(ds4_global['data'].data, 7)))

        # test weighted mean with NANs
        data1 = np.ones(time.shape + lat.shape + lon.shape)
        data2 = np.ones(time.shape + lat.shape + lon.shape)
        data3 = np.random.randint(1000, size=time.shape + lat.shape + lon.shape) * 1.
        data2[0, 0, :] = np.nan
        data2[1, :, :] = np.nan
        data3[0, :, 0] = np.nan
        data3[0, 0, :] = np.nan
        data3[1, :, :] = np.nan
        data3[2, :, :] = np.nan
        data3[2, 0, 0] = 1.
        data3[2, -1, 0] = 2.
        data3[2, -1, -1] = 2.
        data3 = np.ma.masked_invalid(data3)
        weights = np.tile(np.cos(np.radians(lat)), (len(lon), 1)).swapaxes(0, 1).ravel()
        data3_ref = np.ma.filled(np.ma.around(np.ma.average(
            data3.reshape(5, -1),
            axis=-1,
            weights=weights), 7), fill_value=np.nan)
        ds = xr.Dataset(data_vars={'data1': (('time', 'lat', 'lon'), data1),
                                    'data2': (('time', 'lat', 'lon'), data2),
                                    'data3': (('time', 'lat', 'lon'), data3)},
                         coords={'time': time,
                                 'lat': lat,
                                 'lon': ('lon', lon, {'units': 'degree_east'})})
        ds_global = area_weighted_mean(ds, latn='lat')
        self.assertListEqual([1.] * ds.dims['time'], list(np.around(ds_global['data1'].data, 7)))
        self.assertAlmostEqual(1., ds_global.sel(time=0)['data2'].data)
        self.assertTrue(np.isnan(ds_global.sel(time=1)['data2'].data))
        self.assertAlmostEqual(1.66666667, ds_global.sel(time=2)['data3'].data)
        np.testing.assert_array_almost_equal(data3_ref, ds_global['data3'].data)

    # @unittest.skip
    def test_flip_antimeridian(self):

        time = np.arange(5)
        lat = np.arange(-88.75, 90., 2.5)
        lon = np.arange(1.25, 360, 2.5)
        data1 = np.arange(time.size*lat.size*lon.size).reshape(
            time.shape + lat.shape + lon.shape)
        data2 = np.arange(lat.size*lon.size).reshape(lat.shape + lon.shape)

        def _rearange_data(data):
            split_idx = data.size // 2
            return np.concatenate((data[split_idx:], data[:split_idx]))
        _rearange_data = np.vectorize(_rearange_data, signature='(n)->(n)')

        data1_ref = _rearange_data(data1)
        data2_ref = _rearange_data(data2)

        ds = xr.Dataset(data_vars={'data1': (('time', 'lat', 'lon'), data1),
                                   'data2': (('lat', 'lon'), data2),
                                   'data3': (('lon', 'lat', 'time'), data1.swapaxes(0, -1)),
                                   'data4': (('lon', 'lat'), data2.swapaxes(0, 1))},
                        coords={'time': time,
                                'lat': ('lat', lat, {'units': 'degree_north'}),
                                'lon': ('lon', lon, {'units': 'degree_east'})})

        ds_new = ds.copy()
        ds_new['lon'].attrs['units'] = ''
        self.assertRaises(ValueError, flip_antimeridian, ds_new)

        # do nothing
        ds_new = flip_antimeridian(ds, 'Europe')
        for varn in ds.variables:
            np.testing.assert_array_equal(ds_new[varn].data, ds[varn].data)

        ds_new = flip_antimeridian(ds)
        np.testing.assert_array_equal(ds_new['lon'].data, np.arange(-178.75, 180, 2.5))
        np.testing.assert_array_equal(ds_new['data1'].data, data1_ref)
        np.testing.assert_array_equal(ds_new['data2'].data, data2_ref)
        np.testing.assert_array_equal(ds_new['data3'].data, data1_ref.swapaxes(0, -1))
        np.testing.assert_array_equal(ds_new['data4'].data, data2_ref.swapaxes(0, 1))

        # same as DataArray
        ds_new = flip_antimeridian(ds['data1'])
        np.testing.assert_array_equal(ds_new['lon'].data, np.arange(-178.75, 180, 2.5))
        np.testing.assert_array_equal(ds_new.data, data1_ref)

        ds['lon'].data = np.arange(0, 360, 2.5)
        ds['lon'].attrs['units'] = 'degree_east'
        ds_new = flip_antimeridian(ds)
        np.testing.assert_array_equal(ds_new['lon'].data, np.arange(-180, 180, 2.5))
        np.testing.assert_array_equal(ds_new['data1'].data, data1_ref)
        np.testing.assert_array_equal(ds_new['data2'].data, data2_ref)
        np.testing.assert_array_equal(ds_new['data3'].data, data1_ref.swapaxes(0, -1))
        np.testing.assert_array_equal(ds_new['data4'].data, data2_ref.swapaxes(0, 1))

        ds['lon'].data = np.arange(-180, 180, 2.5)
        ds['lon'].attrs['units'] = 'degree_east'

        # do nothing
        ds_new = flip_antimeridian(ds, 'Pacific')
        for varn in ds.variables:
            np.testing.assert_array_equal(ds_new[varn].data, ds[varn].data)

        ds_new = flip_antimeridian(ds, 'Europe')
        np.testing.assert_array_equal(ds_new['lon'].data, np.arange(0, 360, 2.5))
        np.testing.assert_array_equal(ds_new['data1'].data, data1_ref)
        np.testing.assert_array_equal(ds_new['data2'].data, data2_ref)
        np.testing.assert_array_equal(ds_new['data3'].data, data1_ref.swapaxes(0, -1))
        np.testing.assert_array_equal(ds_new['data4'].data, data2_ref.swapaxes(0, 1))

        # asymmetric longitudes
        lon = np.array([0, .1, 179.90, 180, 359.9])
        data = np.arange(lon.size)
        ds = xr.Dataset(data_vars={'data': ('lon', data)},
                        coords={'lon': lon})
        ds_new = flip_antimeridian(ds, lonn='lon')
        np.testing.assert_array_almost_equal(ds_new['lon'].data, np.array([-180, -.1, 0, .1, 179.9]))
        np.testing.assert_array_equal(ds_new['data'].data, np.concatenate((data[3:], data[:3])))

        ds['lon'].data = np.array([0, .1, 180, 180.1, 359.9])
        ds['lon'].attrs['units'] = 'degree_east'
        ds_new = flip_antimeridian(ds)
        np.testing.assert_array_almost_equal(ds_new['lon'].data, np.array([-180, -179.9, -.1, 0, .1]))
        np.testing.assert_array_equal(ds_new['data'].data, np.concatenate((data[2:], data[:2])))

        ds['lon'].data = np.array([-180, -179.9, -.1, 0, 179.9])
        ds['lon'].attrs['units'] = 'degree_east'
        ds_new = flip_antimeridian(ds, 'Europe')
        np.testing.assert_array_equal(ds_new['lon'].data, np.array([0, 179.9, 180, 180.1, 359.9]))
        np.testing.assert_array_equal(ds_new['data'].data, np.concatenate((data[3:], data[:3])))

        ds['lon'].data = np.array([-180, -179.9, 0, .1, 179.9])
        ds['lon'].attrs['units'] = 'degree_east'
        ds_new = flip_antimeridian(ds, 'Europe')
        np.testing.assert_array_equal(ds_new['lon'].data, np.array([0, .1, 179.9, 180, 180.1]))
        np.testing.assert_array_equal(ds_new['data'].data, np.concatenate((data[2:], data[:2])))


    def test_standardize_time(self):
        ds1 = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 1, 1, 12, 0, 0),
                cftime.DatetimeGregorian(2000, 1, 2, 12, 0, 0),
                cftime.DatetimeGregorian(2000, 1, 3, 12, 0, 0)],
                    'merge_dim': [0]})
        ds2 = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 1, 1, 13, 0, 0),
                cftime.DatetimeGregorian(2000, 1, 2, 13, 0, 0),
                cftime.DatetimeGregorian(2000, 1, 3, 13, 0, 0)],
                    'merge_dim': [1]})

        ds = xr.concat([ds1, ds2], dim='merge_dim')
        self.assertNotEqual(ds.dims['time'], ds1.dims['time'])
        self.assertEqual(ds.dims['time'], ds1.dims['time']+ds2.dims['time'])

        self.assertRaises(ValueError, standardize_time, ds1, hour=25)
        self.assertRaises(ValueError, standardize_time, ds1, minute=61)
        self.assertRaises(ValueError, standardize_time, ds1, second=61)
        self.assertRaises(ValueError, standardize_time, ds1, milisecond=101)
        self.assertRaises(ValueError, standardize_time, ds1, wrong_kw=None)

        # trying to standardize day in daily resolved day
        self.assertRaises(ValueError, standardize_time, ds1, day=1)

        ds_ref = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 1, 1, 0, 0, 0),
                cftime.DatetimeGregorian(2000, 1, 2, 0, 0, 0),
                cftime.DatetimeGregorian(2000, 1, 3, 0, 0, 0)]})
        ds1_new = standardize_time(ds1)
        ds2_new = standardize_time(ds2)
        np.testing.assert_array_equal(ds1_new['time'], ds_ref['time'])
        np.testing.assert_array_equal(ds2_new['time'], ds_ref['time'])
        ds = xr.concat([ds1_new, ds2_new], dim='merge_dim')
        np.testing.assert_array_equal(ds['time'], ds_ref['time'])

        ds_ref = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 1, 1, 15, 59, 1),
                cftime.DatetimeGregorian(2000, 1, 2, 15, 59, 1),
                cftime.DatetimeGregorian(2000, 1, 3, 15, 59, 1)]})
        # NOTE: floats are ignored only integer part is taken
        ds1_new = standardize_time(ds1, hour=15.9, minute=59.1, second=1.9)
        ds2_new = standardize_time(ds2, hour=15.9, minute=59.1, second=1.9)
        np.testing.assert_array_equal(ds1_new['time'], ds_ref['time'])
        np.testing.assert_array_equal(ds2_new['time'], ds_ref['time'])
        ds = xr.concat([ds1_new, ds2_new], dim='merge_dim')
        np.testing.assert_array_equal(ds['time'], ds_ref['time'])

        standardize_time(ds1, inplace=True, hour=15.9, minute=59.1, second=1.9)
        np.testing.assert_array_equal(ds1['time'], ds_ref['time'])

        ds1 = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 1, 15, 21, 0, 0),
                cftime.DatetimeGregorian(2000, 2, 16, 22, 0, 0),
                cftime.DatetimeGregorian(2000, 3, 17, 23, 0, 0)]})
        ds_ref = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 1, 1, 21, 0, 0),
                cftime.DatetimeGregorian(2000, 2, 1, 22, 0, 0),
                cftime.DatetimeGregorian(2000, 3, 1, 23, 0, 0)]})
        self.assertRaises(ValueError, standardize_time, ds1, month=1)
        ds1_new = standardize_time(ds1, day=1)  # only change the day
        np.testing.assert_array_equal(ds1_new['time'], ds_ref['time'])

        ds1 = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 1, 15, 21, 0, 0),
                cftime.DatetimeGregorian(2001, 2, 16, 22, 0, 0),
                cftime.DatetimeGregorian(2002, 3, 17, 23, 0, 0)]})
        ds_ref = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 12, 31, 0, 0, 0),
                cftime.DatetimeGregorian(2001, 12, 31, 0, 0, 0),
                cftime.DatetimeGregorian(2002, 12, 31, 0, 0, 0)]})
        ds1_new = standardize_time(ds1)
        np.testing.assert_array_equal(ds1_new['time'], ds_ref['time'])

        ds1 = xr.Dataset(
            coords={'time': [
                cftime.DatetimeGregorian(2000, 1, 1, 21, 0, 0),
                cftime.DatetimeGregorian(2000, 1, 1, 22, 0, 0),
                cftime.DatetimeGregorian(2000, 1, 1, 23, 0, 0)]})
        ds1_new = standardize_time(ds1)
        np.testing.assert_array_equal(ds1_new['time'], ds1['time'])


if __name__ == '__main__':
    logging.disable(logging.CRITICAL)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestUtils)
    unittest.TextTestRunner(verbosity=2).run(suite)
